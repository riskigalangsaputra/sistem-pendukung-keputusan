CREATE TABLE coba
(
    id     varchar(255) NOT NULL,
    nama   varchar(255) NOT NULL,
    email  varchar(255) NOT NULL,
    negara varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB;


insert into coba(id, nama, email, negara)
values ('01', 'noka 0', 'noka0@yopmail.com', 'indonesia'),
       ('02', 'noka 1', 'noka1@yopmail.com', 'indonesia'),
       ('03', 'noka 2', 'noka2@yopmail.com', 'indonesia'),
       ('04', 'noka 3', 'noka3@yopmail.com', 'indonesia'),
       ('05', 'noka 4', 'noka4@yopmail.com', 'indonesia'),
       ('06', 'noka 5', 'noka5@yopmail.com', 'indonesia'),
       ('07', 'noka 6', 'noka6@yopmail.com', 'indonesia'),
       ('08', 'noka 7', 'noka7@yopmail.com', 'indonesia'),
       ('09', 'noka 8', 'noka8@yopmail.com', 'indonesia'),
       ('10', 'noka 9', 'noka9@yopmail.com', 'indonesia'),
       ('11', 'noka 10', 'noka10@yopmail.com', 'indonesia'),
       ('12', 'noka 11', 'noka@11yopmail.com', 'indonesia'),
       ('122', 'noka 12', 'noka12@yopmail.com', 'indonesia'),
       ('13', 'noka 13', 'noka13@yopmail.com', 'indonesia'),
       ('14', 'noka 14', 'noka14@yopmail.com', 'indonesia'),
       ('15', 'noka 15', 'noka15@yopmail.com', 'indonesia'),
       ('16', 'noka 16', 'noka16@yopmail.com', 'indonesia'),
       ('17', 'noka 17', 'noka123@yopmail.com', 'indonesia'),
       ('18', 'noka 18', 'noka324@yopmail.com', 'indonesia'),
       ('19', 'noka 19', 'noka23@yopmail.com', 'amerika'),
       ('20', 'noka 20', 'noka423@yopmail.com', 'amerika'),
       ('21', 'noka 21', 'nokad3@yopmail.com', 'amerika'),
       ('22', 'noka 22', 'noka353d@yopmail.com', 'amerika'),
       ('23', 'noka 23', 'noka52f@yopmail.com', 'amerika'),
       ('24', 'noka 24', 'noka54fh5@yopmail.com', 'amerika'),
       ('25', 'noka 25', 'noka11f4@yopmail.com', 'amerika'),
       ('26', 'noka 26', 'noka4gg5@yopmail.com', 'amerika'),
       ('27', 'noka 27', 'noka55tff@yopmail.com', 'amerika'),
       ('28', 'noka 28', 'nokf43a@yopmail.com', 'amerika'),
       ('29', 'noka 29', 'noka4142@yopmail.com', 'amerika'),
       ('30', 'noka 30', 'nokaff43sd@yopmail.com', 'amerika');