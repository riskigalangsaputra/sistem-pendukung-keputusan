INSERT INTO kriteria(id, nama)
values ("k-01", "Lokasi Pelaksana"),
       ("k-02", "Kedekatan Relasi"),
       ("k-03", "Jenis Kegiatan"),
       ("k-04", "Jumlah Anggaran");

INSERT INTO sub_kriteria (id, id_kriteria, nama)
VALUES ('1', 'k-01', 'Tangerang'),
       ('10', 'k-01', 'Bekasi'),
       ('11', 'k-01', 'DKI Jakarta'),
       ('12', 'k-01', 'Kota Lainnya'),
       ('35', 'k-02', 'BUMD / Pemprov'),
       ('36', 'k-02', 'BUMN'),
       ('37', 'k-02', 'INSTANSI SWASTA'),
       ('38', 'k-02', 'LEMBAGA'),
       ('39', 'k-02', 'KOMUNITAS'),
       ('40', 'k-02', 'UMKM'),
       ('42', 'k-03', 'Pendidikan'),
       ('43', 'k-03', 'Keagamaan'),
       ('44', 'k-03', 'Bakti Sosial'),
       ('45', 'k-03', 'Pemberdayaan Lingkungan'),
       ('46', 'k-03', 'Bencana Alam'),
       ('47', 'k-03', 'Wabah Penyakit');

insert into prefensi (id, nama, bobot)
values ("01", "Sama pentingnya", 1),
       ("02", "Ragu-ragu antara dua nilai yang berdekatan", 2),
       ("03", "Sedikit lebih penitng", 3),
       ("04", "Ragu-ragu antara dua nilai yang berdekatan", 4),
       ("05", "Jelas lebih penting", 5),
       ("06", "Ragu-ragu antara dua nilai yang berdekatan", 6),
       ("07", "Sangat jelas penting", 7),
       ("08", "Ragu-ragu antara dua nilai yang berdekatan", 8),
       ("09", "Mutlak lebih penting", 9);

insert into saldo (id, jumlah)
values ('01', 80000000);



