INSERT INTO c_security_permission (id, permission_label, permission_value)
VALUES ('1_1_view_managemenet_user', 'view management user', 'ROLE_VIEW_MANAGEMENT_USER'),
       ('1_2_edit_managemenet_user', 'edit management user', 'ROLE_EDIT_MANAGEMENT_USER'),
       ('2_1_view_approve_pemohon', 'view approve pemohon', 'ROLE_VIEW_APPROVE_PEMOHON'),
       ('2_2_edit_approve_pemohon', 'edit approve pemohon', 'ROLE_EDIT_APPROVE_PEMOHON'),
       ('3_1_view_pengajuan', 'view pengajuan', 'ROLE_VIEW_PENGAJUAN'),
       ('3_2_edit_pengajuan', 'edit pengajuan', 'ROLE_EDIT_PENGAJUAN'),
       ('4_1_view_perbandingan', 'view perbandingan', 'ROLE_VIEW_PERBANDINGAN'),
       ('4_2_edit_perbandingan', 'edit perbandingan', 'ROLE_EDIT_PERBANDINGAN'),
       ('5_1_view_master', 'view master', 'ROLE_VIEW_MASTER'),
       ('5_2_edit_master', 'edit master', 'ROLE_EDIT_MASTER'),
       ('6_1_view_request_pengajuan', 'view request pengajuan', 'ROLE_VIEW_REQUEST_PENGAJUAN'),
       ('6_2_edit_request_pengajuan', 'edit request pengajuan', 'ROLE_EDIT_REQUEST_PENGAJUAN');

INSERT INTO c_security_role (id, description, name)
VALUES ('role_for_admin', 'Admin', 'ADMINISTRATOR'),
       ('role_for_manager', 'Manager', 'MANAGER'),
       ('role_for_pemohon', 'Pemohon', 'PEMOHON');

INSERT INTO c_security_role_permission (id_role, id_permission)
VALUES ('role_for_admin', '1_1_view_managemenet_user'),
       ('role_for_admin', '1_2_edit_managemenet_user'),
       ('role_for_admin', '4_1_view_perbandingan'),
       ('role_for_admin', '4_2_edit_perbandingan'),
       ('role_for_admin', '5_1_view_master'),
       ('role_for_admin', '5_2_edit_master'),
       ('role_for_admin', '6_1_view_request_pengajuan'),
       ('role_for_admin', '6_2_edit_request_pengajuan'),
       ('role_for_manager', '6_1_view_request_pengajuan'),
       ('role_for_manager', '6_2_edit_request_pengajuan'),
       ('role_for_pemohon', '3_1_view_pengajuan'),
       ('role_for_pemohon', '3_2_edit_pengajuan');

INSERT INTO c_security_user (id, username, fullname, active, id_role, user_type)
VALUES ('admin', 'admin.csr@yopmail.com', 'Admin Csr', true, 'role_for_admin', 'ADMIN'),
       ('manager', 'manager.csr@yopmail.com', 'Manager Csr', true, 'role_for_manager', 'MANAGER');

INSERT INTO c_security_user_password (id_user, user_password)
VALUES ('admin', '$2b$10$jr7IQOt65ljFl3g9fdggxeUwLujqWBTFciTiuUzM8zJSJYXovM4wG');

INSERT INTO c_security_user_password (id_user, user_password)
VALUES ('manager', '$2b$10$jr7IQOt65ljFl3g9fdggxeUwLujqWBTFciTiuUzM8zJSJYXovM4wG');
