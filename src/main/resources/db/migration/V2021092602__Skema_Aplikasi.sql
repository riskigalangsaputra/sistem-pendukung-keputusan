CREATE TABLE pemohon
(
    id         varchar(255) NOT NULL,
    id_user    varchar(255) NOT NULL,
    kode       varchar(255) NOT NULL,
    nama       varchar(36)  NOT NULL,
    email      varchar(36)  NOT NULL,
    no_telepon varchar(36)  NOT NULL,
    alamat     varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE kriteria
(
    id   varchar(255) NOT NULL,
    nama varchar(50)  NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE (nama)
) ENGINE=InnoDB;

CREATE TABLE sub_kriteria
(
    id          varchar(255) NOT NULL,
    id_kriteria varchar(255) NOT NULL,
    nama        varchar(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE (nama)
) ENGINE=InnoDB;

CREATE TABLE prefensi
(
    id    varchar(255) NOT NULL,
    nama  varchar(100) NOT NULL,
    bobot int (10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE perbandingan_kriteria
(
    id               varchar(255)   NOT NULL,
    id_kriteria_satu varchar(255)   NOT NULL,
    id_kriteria_dua  varchar(255)   NOT NULL,
    nilai            decimal(19, 1) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE pvector_kriteria
(
    id          varchar(255) NOT NULL,
    id_kriteria varchar(255) NOT NULL,
    nilai       float        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `pengajuan`
(
    `id`                        varchar(255) NOT NULL,
    `kode`                      varchar(255) NOT NULL,
    `tanggal_pengajuan`         datetime     NOT NULL,
    `nama_instansi`             varchar(50)  NOT NULL,
    `id_kedekatan_relasi`       varchar(255) NOT NULL,
    `alamat_instansi`           varchar(255) NOT NULL,
    `nama_pic`                  varchar(255) NOT NULL,
    `kontak_pic`                varchar(255) NOT NULL,
    `gambaran_kegiatan`         varchar(255) NOT NULL,
    `id_jenis_kegiatan`         varchar(255) NOT NULL,
    `id_lokasi_pelaksana`       varchar(255) NOT NULL,
    `id_pemohon`                varchar(255) NOT NULL,
    `pilihan_anggaran`          varchar(50),
    `nominal_anggaran`          varchar(50),
    `jumlah_anggaran_diperoleh` decimal(19, 2),
    `status_pengajuan`          varchar(25)  NOT NULL,
    `surat_pengantar`           varchar(255),
    `proposal_kegiatan`         varchar(255),
    `bobot`                     decimal(19, 3),
    `alasan_penolakan`          varchar(255),
    PRIMARY KEY (`id`),
    UNIQUE (kode)
) ENGINE=InnoDB;

CREATE TABLE perbandingan_alternatif
(
    id                 varchar(255) NOT NULL,
    id_kriteria        varchar(255) NOT NULL,
    id_alternatif_satu varchar(255) NOT NULL,
    id_alternatif_dua  varchar(255) NOT NULL,
    nilai              float        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE pv_alternatif
(
    id           varchar(225) NOT NULL,
    id_pengajuan varchar(225) NOT NULL,
    id_kriteria  varchar(225) NOT NULL,
    nilai        float        NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE ranking
(
    id           varchar(225) NOT NULL,
    id_pengajuan varchar(225) NOT NULL,
    nilai        float        NOT NULL,
    PRIMARY KEY (id)

)ENGINE=InnoDB;

CREATE TABLE saldo
(
    id       varchar(225)   NOT NULL,
    `jumlah` decimal(19, 1) NOT NULL,
    PRIMARY KEY (id)

)ENGINE=InnoDB;

-- sub kriteria
alter table sub_kriteria
    add constraint FKnqcv2qdac1phe92iqn72udyt
        foreign key (id_kriteria)
            references kriteria (id);

-- Pemohon --
alter table pemohon
    add constraint UK_9gamvc5lx9mwkiu3wkaj5xuq3 unique (kode);

alter table pemohon
    add constraint UK_9gamvc5lx9mwkiu3wkaj5x0oi unique (email);

alter table pemohon
    add constraint FKnqcv2qdac1phe92iqnyi6p02
        foreign key (id_user)
            references c_security_user (id);

-- Pengajuan --
alter table pengajuan
    add constraint FKnqcv2qdac1phe92iqnyi6n1n
        foreign key (id_pemohon)
            references pemohon (id);

alter table pengajuan
    add constraint FKnqcv2qdac1phe92iqny09olk
        foreign key (id_kedekatan_relasi)
            references sub_kriteria (id);

alter table pengajuan
    add constraint FKnqcv2qdac1phe92iqny9oid7
        foreign key (id_jenis_kegiatan)
            references sub_kriteria (id);

alter table pengajuan
    add constraint FKnqcv2qdac1phe92iqnyhnc87
        foreign key (id_lokasi_pelaksana)
            references sub_kriteria (id);

-- Perbandingan Kriteria --
alter table perbandingan_kriteria
    add constraint FKnqcv2qdac1phe92iqnyhldo9
        foreign key (id_kriteria_satu)
            references kriteria (id);

alter table perbandingan_kriteria
    add constraint FKnqcv2qdac1phe92iqny098io
        foreign key (id_kriteria_dua)
            references kriteria (id);

alter table pvector_kriteria
    add constraint FKnqcv2qdac1phe92iqny1425d
        foreign key (id_kriteria)
            references kriteria (id);

-- Perbandingan Alternatif --
alter table perbandingan_alternatif
    add constraint FKnqcv2qdac1phe92iqny8idku
        foreign key (id_kriteria)
            references kriteria (id);

alter table perbandingan_alternatif
    add constraint FKnqcv2qdac1phe92iqnydddki
        foreign key (id_alternatif_satu)
            references pengajuan (id);

alter table perbandingan_alternatif
    add constraint FKnqcv2qdac1phe92iqn889ieu
        foreign key (id_alternatif_dua)
            references pengajuan (id);

alter table pv_alternatif
    add constraint FKnqcv2qdac1phe92iqn8iiis8
        foreign key (id_pengajuan)
            references pengajuan (id);

alter table pv_alternatif
    add constraint FKnqcv2qdac1phe92iqn8987ui
        foreign key (id_kriteria)
            references kriteria (id);

alter table ranking
    add constraint FKnqcv2qdac1phe92iqn882iu7
        foreign key (id_pengajuan)
            references pengajuan (id);