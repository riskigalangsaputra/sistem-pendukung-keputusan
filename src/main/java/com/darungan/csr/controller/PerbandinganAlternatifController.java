package com.darungan.csr.controller;

import com.darungan.csr.dtos.HasilPerbandinganDto;
import com.darungan.csr.dtos.PerbandinganDto;
import com.darungan.csr.services.AlternatifService;
import com.darungan.csr.services.KriteriaService;
import com.darungan.csr.services.PrefensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/perbandingan_alternatif")
public class PerbandinganAlternatifController {

    private static final String FORM = "perbandingan_alternatif/form";
    private static final String HASIL = "perbandingan_alternatif/hasil";

    @Autowired
    private KriteriaService kriteriaService;

    @Autowired
    private AlternatifService alternatifService;

    @Autowired
    private PrefensiService prefensiService;

    @GetMapping
    public String form(@RequestParam(required = true) int index, ModelMap mm) {
        PerbandinganDto perbandinganDto = new PerbandinganDto();
        perbandinganDto.setIndex(index);
        perbandinganDto.setPembanding(kriteriaService.getKriteriaByIndex(index));

        mm.addAttribute("perbandinganDto", perbandinganDto);
        mm.addAttribute("datas", alternatifService.perbandinganAlternatifList());
        mm.addAttribute("prefensis", prefensiService.getPrefensiIterable());
        return FORM;
    }

    @PostMapping
    public String proses(PerbandinganDto perbandinganDto, ModelMap mm, RedirectAttributes redir) {
        try {

            perbandinganDto.setIndex(perbandinganDto.getIndex() + 1);
            if (perbandinganDto.getIndex() == 4) {
                redir.addFlashAttribute("rangking", true);
                redir.addFlashAttribute("lanjut", false);
            } else {
                redir.addFlashAttribute("rangking", false);
                redir.addFlashAttribute("lanjut", true);
            }

            HasilPerbandinganDto hasil = alternatifService.prosesPerbandinganAlternatif(perbandinganDto);
            redir.addFlashAttribute("hasilDto", hasil);
            redir.addFlashAttribute("datas", alternatifService.getAlternatifList());
            redir.addFlashAttribute("perbandinganDto", perbandinganDto);
            redir.addFlashAttribute("successMessage", "Perbandingan data antar alternatif berhasil.");
            return "redirect:/perbandingan_alternatif/hasil_perhitungan";
        } catch (Exception e) {
            e.printStackTrace();
            mm.addAttribute("errorMessage", "Perbandingan data antar alternatif gagal.");
            mm.addAttribute("perbandinganDto", perbandinganDto);
            mm.addAttribute("datas", alternatifService.perbandinganAlternatifList());
            mm.addAttribute("prefensis", prefensiService.getPrefensiIterable());
            return FORM;
        }
    }

    @GetMapping("/hasil_perhitungan")
    public String hasilPerhitungan() {
        return HASIL;
    }

}
