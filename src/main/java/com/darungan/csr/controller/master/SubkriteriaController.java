package com.darungan.csr.controller.master;

import com.darungan.csr.entity.SubKriteria;
import com.darungan.csr.services.SubKriteriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/master/sub_kriteria")
public class SubkriteriaController {

    @Autowired
    private SubKriteriaService subKriteriaService;

    @GetMapping
    public String list(ModelMap modelMap, @PageableDefault(sort = "nama", direction = Sort.Direction.ASC) Pageable pageable) {
        modelMap.addAttribute("datas", subKriteriaService.getSubKriteriaPage(pageable));
        return "master/sub_kriteria/list";
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = false) String id, ModelMap modelMap) {
        modelMap.addAttribute("subKriteria", new SubKriteria());
        return "master/sub_kriteria/form";
    }

    @PostMapping("/fomr")
    public String save(@Valid SubKriteria subKriteria, ModelMap modelMap, RedirectAttributes redir) {
        try {
            subKriteriaService.save(subKriteria);
            redir.addFlashAttribute("successMessage", "Data berhasil disimpan");
            return "redirect:/master/sub_kriteria";
        } catch (Exception e) {
            e.printStackTrace();
            redir.addFlashAttribute("errorMessage", e.getMessage());
            modelMap.addAttribute("subKriteria", subKriteria);
            return "master/sub_kriteria/form";
        }
    }
}
