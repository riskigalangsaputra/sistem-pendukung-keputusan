package com.darungan.csr.controller;

import com.darungan.csr.dtos.SaldoDto;
import com.darungan.csr.dtos.TolakDto;
import com.darungan.csr.services.PengajuanService;
import com.darungan.csr.utils.TypeFIle;
import com.darungan.csr.utils.UploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.net.MalformedURLException;

@Controller
@RequestMapping("/request_pengajuan")
public class RequestPengajuanController {

    private static final String LIST = "request_pengajuan/list";

    private static final String TOLAK = "request_pengajuan/tolak";

    private static final String SALDO = "request_pengajuan/input_saldo";

    private static final String DETAIL = "request_pengajuan/detail";

    @Autowired
    private UploadHelper uploadHelper;

    @Autowired
    private PengajuanService pengajuanService;

    @GetMapping
    public String list(@PageableDefault(sort = "tanggalPengajuan", direction = Sort.Direction.ASC) Pageable pageable, ModelMap mm) {
        mm.addAttribute("datas", pengajuanService.getPengajuanPage(pageable));
        return LIST;
    }

    @GetMapping("/approve")
    public String approve(@RequestParam(required = true) String id, RedirectAttributes redir, ModelMap modelMap) {
        try {
            pengajuanService.approve(id);
            redir.addFlashAttribute("successMessage", "Pengajuan telah di setujui.");
            return "redirect:/request_pengajuan";
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.addAttribute("errorMessage", e.getMessage());
            return LIST;
        }
    }

    @GetMapping("/tolak")
    public String tolakForm(@RequestParam(required = true) String id, ModelMap modelMap) {
        TolakDto tolakDto = new TolakDto();
        tolakDto.setId(id);
        modelMap.addAttribute("tolakDto", tolakDto);
        return TOLAK;
    }

    @GetMapping("/detail")
    private String detail(@RequestParam(required = true) String id, ModelMap modelMap) {
        modelMap.addAttribute("pengajuan", pengajuanService.getPengajuanDtoById(id));
        return DETAIL;
    }

    @PostMapping("/tolak")
    public String tolakProses(@Valid TolakDto tolakDto, RedirectAttributes redir, ModelMap mm) {
        try {
            pengajuanService.tolak(tolakDto);
            redir.addFlashAttribute("successMessage", "Pengajuan telah di tolak test.");
            return "redirect:/request_pengajuan";
        } catch (Exception e) {
            e.printStackTrace();
            mm.addAttribute("tolakDto", tolakDto);
            mm.addAttribute("errorMessage", e.getMessage());
            return LIST;
        }
    }

    @GetMapping("/input_dana")
    public String inputDanaForm(@RequestParam(required = true) String id, ModelMap modelMap) {
        SaldoDto saldoDto = new SaldoDto();
        saldoDto.setId(id);
        modelMap.addAttribute("saldoDto", saldoDto);
        return SALDO;
    }

    @PostMapping("/input_dana")
    public String inputDana(@Valid SaldoDto saldoDto, ModelMap modelMap, RedirectAttributes redir) {
        try {
            pengajuanService.inputDana(saldoDto);
            redir.addFlashAttribute("successMessage", "Pengajuan selesai.");
            return "redirect:/request_pengajuan";
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.addAttribute("saldoDto", saldoDto);
            modelMap.addAttribute("errorMessage", e.getMessage());
            return SALDO;
        }
    }

    @ResponseBody
    @GetMapping("/download_pengantar/{filename:.+}")
    public ResponseEntity<Resource> downloadDocumentPengantar(@PathVariable String filename) {
        try {
            Resource file = uploadHelper.loadAsResource(filename, TypeFIle.SURAT_PENGANTAR);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (MalformedURLException ex) {
            return null;
        }
    }

    @ResponseBody
    @GetMapping("/download_proposal/{filename:.+}")
    public ResponseEntity<Resource> downloadDocumentProposal(@PathVariable String filename) {
        try {
            Resource file = uploadHelper.loadAsResource(filename, TypeFIle.PROPOSAL_KEGIATAN);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (MalformedURLException ex) {
            return null;
        }
    }
}
