package com.darungan.csr.controller;

import com.darungan.csr.dtos.BaseResponseDto;
import com.darungan.csr.dtos.PeringkatDto;
import com.darungan.csr.services.AlternatifService;
import com.darungan.csr.services.HasilAhpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/alternatif")
public class AlternatifRestController {

    @Autowired
    private HasilAhpService hasilAhpService;

    @GetMapping("/rangking")
    public List<PeringkatDto> getDataAlternatif() {
        try {
            BaseResponseDto responseDto = new BaseResponseDto();
            responseDto.setResponseMessage("success");
            responseDto.setResponseCode("00");
            responseDto.setData(hasilAhpService.getPeringkatDtoList());
            return hasilAhpService.getPeringkatDtoList();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

    }
}
