package com.darungan.csr.controller;

import com.darungan.csr.dtos.PemohonDto;
import com.darungan.csr.services.PemohonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/register")
public class RegisterController {

    public static final String FORM = "register";

    @Autowired
    private PemohonService pemohonService;

    @GetMapping
    public String getForm(ModelMap mm) {
        mm.addAttribute("pemohonDto", new PemohonDto());
        return FORM;
    }

    @PostMapping
    public String saveForm(@Valid PemohonDto pemohonDto, BindingResult errors, ModelMap mm, RedirectAttributes redir) {

        pemohonService.validateRegistrasi(pemohonDto, errors);
        if (errors.hasErrors()) {
            log.error("ERROR INVALID [{}]", errors.getAllErrors());
            mm.addAttribute("pemohonDto", pemohonDto);
            return FORM;
        }

        try {
            pemohonService.registrasiPemohon(pemohonDto);
            redir.addFlashAttribute("successMessage", "Registrasi akun berhasil, Silahkan login untuk memulai aplikasi.");
            return "redirect:/login";
        } catch (Exception e) {
            e.printStackTrace();
            log.info("ERROR [{}]", e.getMessage());
            mm.addAttribute("errorMessage", "Registrasi akun gagal, Silahkan registrasi kembali.");
            return FORM;
        }
    }
}
