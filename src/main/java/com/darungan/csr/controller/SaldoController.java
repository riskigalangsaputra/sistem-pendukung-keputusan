package com.darungan.csr.controller;

import com.darungan.csr.entity.Saldo;
import com.darungan.csr.services.SaldoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/master/saldo")
public class SaldoController {

    private static final String LIST = "master/saldo/list";
    private static final String FORM = "master/saldo/form";

    @Autowired
    private SaldoService saldoService;

    @GetMapping
    public String list(ModelMap modelMap) {
        modelMap.addAttribute("datas", saldoService.getSaldoList());
        return LIST;
    }

    @GetMapping("/form")
    public String form(@RequestParam(required = true) String id, ModelMap modelMap) {
        Saldo saldo = new Saldo();
        saldo.setId(id);
        modelMap.addAttribute("saldo", saldo);
        return FORM;
    }

    @GetMapping("/reset_saldo")
    public String resetSaldo(@RequestParam(required = true) String id, RedirectAttributes redir) {
        try {
            saldoService.resetNominal(id);
            redir.addFlashAttribute("successMessage", "Saldo berhasil di reset.");
        } catch (Exception e) {
            redir.addFlashAttribute("errorMessage", "Saldo gagal di reset.");
        }
        return "redirect:/master/saldo";
    }

    @PostMapping("/form")
    public String save(@Valid Saldo saldo, ModelMap modelMap, RedirectAttributes redir) {
        try {
            saldoService.save(saldo);
            redir.addFlashAttribute("successMessage", "Berhasil disimpana");
            return "redirect:/master/saldo";
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.addAttribute("errorMessage", "Gagal disimpan !");
            return FORM;
        }
    }
}
