package com.darungan.csr.controller;

import com.darungan.csr.dao.UserDao;
import com.darungan.csr.entity.User;
import com.darungan.csr.services.AlternatifService;
import com.darungan.csr.services.PemohonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class DashboardController {

    @Autowired
    private AlternatifService alternatifService;

    @Autowired
    private PemohonService pemohonService;

    @Autowired
    private UserDao userDao;

    @GetMapping
    public String showDashboard(ModelMap modelMap, Principal principal) {

        String $_asd = "";

        Optional<User> user = userDao.findByUsername(principal.getName());
        if (User.UserType.PEMOHON.equals(user.get().getUserType())) {
            modelMap.addAttribute("waitingApprove", alternatifService.countWaitingApprovePemohon(user.get().getUsername()));
            modelMap.addAttribute("approved", alternatifService.countApprovedPemohon(user.get().getUsername()));
            modelMap.addAttribute("reject", alternatifService.countRejectPemohon(user.get().getUsername()));
            modelMap.addAttribute("selesai", alternatifService.countSelesaiPemohon(user.get().getUsername()));
            modelMap.addAttribute("pemohons", pemohonService.getListPemohon());
        } else {
            modelMap.addAttribute("waitingApprove", alternatifService.countWaitingApprove());
            modelMap.addAttribute("approved", alternatifService.countApproved());
            modelMap.addAttribute("reject", alternatifService.countReject());
            modelMap.addAttribute("selesai", alternatifService.countSelesai());
            modelMap.addAttribute("pemohons", pemohonService.getListPemohon());
        }
        return "dashboard/dashboard";
    }

}
