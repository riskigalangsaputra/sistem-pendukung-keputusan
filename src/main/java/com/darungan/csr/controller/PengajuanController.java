package com.darungan.csr.controller;

import com.darungan.csr.dtos.PengajuanDto;
import com.darungan.csr.entity.Pengajuan;
import com.darungan.csr.services.PengajuanService;
import com.darungan.csr.services.SubKriteriaService;
import com.darungan.csr.services.UploadService;
import com.darungan.csr.utils.UploadHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.net.MalformedURLException;
import java.security.Principal;
import java.util.StringTokenizer;

@Controller
@RequestMapping("/pengajuan")
public class PengajuanController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String LIST = "pengajuan/list";
    private static final String FORM = "pengajuan/form";
    private static final String DETAIL = "pengajuan/detail";

    @Autowired
    private SubKriteriaService subKriteriaService;

    @Autowired
    private PengajuanService pengajuanService;

    @Autowired
    private UploadHelper uploadHelper;

    @GetMapping
    public String list(@PageableDefault(sort = "tanggalPengajuan", direction = Sort.Direction.DESC) Pageable pageable,
                       ModelMap mm, Principal principal) {
        mm.addAttribute("datas", pengajuanService.getPengajuanPageByUser(principal.getName(), pageable));
        return LIST;
    }

    @GetMapping("/form")
    private String form(@RequestParam(required = false) String id, ModelMap modelMap) {
        modelMap.addAttribute("pengajuanDto", pengajuanService.getPengajuanDto(id));
        returnData(modelMap);
        return FORM;
    }

    @GetMapping("/detail")
    private String detail(@RequestParam(required = true) String id, ModelMap modelMap) {
        modelMap.addAttribute("pengajuan", pengajuanService.getPengajuanDtoById(id));
        return DETAIL;
    }

    @PostMapping("/form")
    public String processForm(@Valid PengajuanDto pengajuanDto, BindingResult bindingResult,
                              ModelMap modelMap, RedirectAttributes redir, Principal principal,
                              MultipartFile filePengantar, MultipartFile fileProposal) {

        if (bindingResult.hasErrors()) {
            logger.error("ERROR INVALID [{}]", bindingResult.getAllErrors());
            modelMap.addAttribute("pengajuanDto", pengajuanDto);
            returnData(modelMap);
            return FORM;
        }

        String suratPengantar = "";
        String proposalKegiatan = "";
        if (filePengantar != null && !filePengantar.isEmpty() && fileProposal != null && !fileProposal.isEmpty()) {
            logger.info("uploadted");
            try {
                suratPengantar = uploadHelper.saveFilePengantar(filePengantar, pengajuanDto.getNamaInstansi());
                proposalKegiatan = uploadHelper.saveFileProposal(fileProposal, pengajuanDto.getNamaInstansi());
            } catch (Exception e) {
                modelMap.addAttribute("errorMessage", e.getMessage());
                modelMap.addAttribute("pengajuanDto", pengajuanDto);
                returnData(modelMap);
                return FORM;
            }
        }

        try {
            pengajuanService.savePengajuan(pengajuanDto, principal.getName(), suratPengantar, proposalKegiatan);
            redir.addFlashAttribute("successMessage", "Data berhasil di simpan.");
            return "redirect:/pengajuan";
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.addAttribute("errorMessage", e.getMessage());
            modelMap.addAttribute("pengajuanDto", pengajuanDto);
            returnData(modelMap);
            return FORM;
        }
    }

    @GetMapping("/delete")
    private String delete(@RequestParam(required = true) String id, ModelMap modelMap, RedirectAttributes redir) {
        try {
            pengajuanService.delete(id);
            redir.addFlashAttribute("successMessage", "Data berhasil di delete.");
        } catch (Exception e) {
            modelMap.addAttribute("errorMessage", e.getMessage());
        }
        return "redirect:/pengajuan";
    }

    private void returnData(ModelMap modelMap) {
        modelMap.addAttribute("lokasis", subKriteriaService.getLokasiKegiatanList());
        modelMap.addAttribute("kedekatans", subKriteriaService.getKedekatanRelasiList());
        modelMap.addAttribute("jenis", subKriteriaService.getJenisKegiatanList());
    }
}
