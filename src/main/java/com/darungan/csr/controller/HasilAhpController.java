package com.darungan.csr.controller;

import com.darungan.csr.services.AlternatifService;
import com.darungan.csr.services.HasilAhpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hasil_ahp")
public class HasilAhpController {

    @Autowired
    private HasilAhpService hasilAhpService;

    @Autowired
    private AlternatifService alternatifService;

    @GetMapping
    public String hasil(ModelMap modelMap) {
        hasilAhpService.hitungPeringkat();
        modelMap.addAttribute("alternatifs", alternatifService.getAlternatifList());
        modelMap.addAttribute("pvKriteria", hasilAhpService.getPvKriteriaListDto());
        modelMap.addAttribute("pvAlternatif", hasilAhpService.getPvAternatifList());
        modelMap.addAttribute("nilaiPvAlternatif", hasilAhpService.getPvAlternatifNilai());
        modelMap.addAttribute("peringkat", hasilAhpService.getPeringkatDtoList());
        modelMap.addAttribute("total", hasilAhpService.getTotal());
        return "hasil_ahp/list";
    }
}
