package com.darungan.csr.controller.api;

import com.darungan.csr.entity.Coba;
import com.darungan.csr.services.CobaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author galang
 */

@RestController
public class CobaRestController {


    @Autowired private CobaService cobaService;

    @GetMapping("/api/coba")
    public Page<Coba> findAll(int page, int size, @RequestParam Map<String, String> params) {
        return cobaService.findAll(page, size, params);
    }
}
