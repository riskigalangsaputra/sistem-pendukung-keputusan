package com.darungan.csr.controller;

import com.darungan.csr.dtos.HasilPerbandinganDto;
import com.darungan.csr.dtos.PerbandinganDto;
import com.darungan.csr.services.KriteriaService;
import com.darungan.csr.services.PrefensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@RequestMapping("/perbandingan_kriteria")
@Controller
public class PerbandinganKriteriaController {

    private static final String FORM = "perbandingan_kriteria/form";
    private static final String HASIL = "perbandingan_kriteria/hasil_perbandingan";

    @Autowired
    private KriteriaService kriteriaService;

    @Autowired
    private PrefensiService prefensiService;

    @GetMapping
    public String form(ModelMap mm) {
        mm.addAttribute("perbandinganDto", new PerbandinganDto());
        returnData(mm);
        return FORM;
    }

    @PostMapping
    public String proses(PerbandinganDto perbandinganDto, ModelMap mm, RedirectAttributes redir) {
        try {
            HasilPerbandinganDto hasil = kriteriaService.prosesPerbandingan(perbandinganDto);
            redir.addFlashAttribute("hasilDto", hasil);
            redir.addFlashAttribute("datas", kriteriaService.getKriteriaList());
            redir.addFlashAttribute("successMessage", "Perbandingan data antar kriteria berhasil.");
            return "redirect:/perbandingan_kriteria/hasil_perhitungan";
        } catch (Exception e) {
            e.printStackTrace();
            mm.addAttribute("errorMessage", "Perbandingan data antar kriteria gagal.");
            mm.addAttribute("perbandinganDto", perbandinganDto);
            returnData(mm);
            return FORM;
        }
    }

    @GetMapping("/hasil_perhitungan")
    public String hasilPerhitungan() {
        return HASIL;
    }

    private void returnData(ModelMap modelMap) {
        modelMap.addAttribute("datas", kriteriaService.perbandinganKriteriaList());
        modelMap.addAttribute("prefensis", prefensiService.getPrefensiIterable());
    }
}
