package com.darungan.csr.controller;

import com.darungan.csr.dao.UserDao;
import com.darungan.csr.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping("/change_password")
public class ChangePasswordController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping
    public String showForm() {
        return "change_password/form";
    }

    @PostMapping
    public String processChangingPassword(@RequestParam String currentPassword, @RequestParam String password, Principal principal, ModelMap modelMap) {

        Optional<User> userOp = userDao.findByUsername(principal.getName());
        if (userOp.isPresent()) {
            User user = userOp.get();
            if (passwordEncoder.matches(currentPassword, user.getUserPassword().getPassword())) {
                user.getUserPassword().setPassword(passwordEncoder.encode(password));
                userDao.save(user);
                modelMap.addAttribute("successMessage", "Password berhasil diubah");
            } else {
                modelMap.addAttribute("errorMessage", "Masukan password saat ini salah");
            }
        }
        return "change_password/form";
    }


}
