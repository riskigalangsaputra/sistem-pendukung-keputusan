package com.darungan.csr.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.UUID;

@Component
@Slf4j
public class UploadHelper {

    public static final String FILE_PENGANTAR = "surat-pengantar";
    public static final String FILE_PROPOSAL = "proposal-kegiatan";

    @Value("${app.folder.upload}")
    private String uploadFolder;

    public String getPengantarFolder() {
        return uploadFolder + File.separator + FILE_PENGANTAR;
    }

    public String getProposalFolder() {
        return uploadFolder + File.separator + FILE_PROPOSAL;
    }

    public String saveFilePengantar(MultipartFile multipartFile, String namaPemohon) {

        if (multipartFile.getOriginalFilename().isEmpty()) {
            throw new IllegalStateException("Error save " + namaPemohon + " Surat Pengantar. Filename empty");
        }

        String destinationFolder = getPengantarFolder();
        String extension = parseExtension(multipartFile.getOriginalFilename());
        String filename = UUID.randomUUID().toString() + "-" + namaPemohon + extension;
        writeFile(destinationFolder, filename, multipartFile);
        return filename;
    }

    public String saveFileProposal(MultipartFile multipartFile, String namaPemohon) {

        if (multipartFile.getOriginalFilename().isEmpty()) {
            throw new IllegalStateException("Error save " + namaPemohon + " Surat Proposal. Filename empty");
        }

        String destinationFolder = getProposalFolder();
        String extension = parseExtension(multipartFile.getOriginalFilename());
        String filename = UUID.randomUUID().toString() + "-" + namaPemohon + extension;
        writeFile(destinationFolder, filename, multipartFile);
        return filename;
    }

    private void writeFile(String destinationFolder, String filename, MultipartFile multipartFile) {
        try {
            new File(destinationFolder).mkdirs();
            String destinationFile = destinationFolder + File.separator + filename;
            log.debug("Destination file : {}", destinationFile);
            multipartFile.transferTo(new File(destinationFile));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new IllegalStateException("Error save file " + filename);
        }
    }

    private String parseExtension(String filename) {
        return filename.substring(filename.lastIndexOf("."));
    }

    private Path load(String filename, TypeFIle typeFile) {
        String FOLDER_FILE = "";
        log.info("{}", typeFile);

        if (TypeFIle.SURAT_PENGANTAR.equals(typeFile)) {
            FOLDER_FILE = FILE_PENGANTAR;
        } else {
            FOLDER_FILE = FILE_PROPOSAL;
        }
        log.info("{}", FOLDER_FILE);
        return new File(uploadFolder + File.separator + FOLDER_FILE).toPath().resolve(filename);
    }

    public Resource loadAsResource(String filename, TypeFIle typeFile) throws MalformedURLException {
        Path filePath = load(filename, typeFile);
        log.info("{}", filePath);
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists() || resource.isReadable()) {
            return resource;
        } else {
            throw new MalformedURLException("Could not read file: " + filename);
        }
    }
}
