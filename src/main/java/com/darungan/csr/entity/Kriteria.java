package com.darungan.csr.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data @Entity
public class Kriteria extends BaseEntity{

    @NotNull
    @NotEmpty(message = "Nama Product Type Tidak boleh kosong")
    @Column(unique = true)
    private String nama;
}
