package com.darungan.csr.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data @Entity
public class Ir {

    @Id
    @NotNull
    private int jumlah;

    @NotNull
    private float nilai;
}
