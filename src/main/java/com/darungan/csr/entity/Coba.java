package com.darungan.csr.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * @author galang
 */
@Getter
@Setter
@Entity
public class Coba extends BaseEntity {

    @NotNull
    private String nama;

    @NotNull
    private String email;

    @NotNull
    private String negara;

}
