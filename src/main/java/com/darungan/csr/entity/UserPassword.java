package com.darungan.csr.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "c_security_user_password")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPassword extends BaseEntity {

    @OneToOne
    @MapsId
    @JoinColumn(name = "id_user", nullable = false, columnDefinition = "varchar(36)")
    private User user;

    @Column(nullable = false, name = "user_password")
    private String password;

}
