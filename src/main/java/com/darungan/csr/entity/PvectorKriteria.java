package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class PvectorKriteria extends BaseEntity {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria", nullable = false)
    private Kriteria kriteria;

    @NotNull
    private float nilai;
}
