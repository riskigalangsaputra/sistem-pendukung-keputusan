package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data @Entity
public class SubKriteria extends BaseEntity{

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria", nullable = false)
    private Kriteria kriteria;

    @NotNull @NotEmpty
    private String nama;
}
