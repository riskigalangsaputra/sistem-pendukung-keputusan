package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class PerbandinganKriteria extends BaseEntity{

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria_satu", nullable = false)
    private Kriteria kriteriaSatu;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria_dua", nullable = false)
    private Kriteria kriteriaDua;

    @NotNull
    private BigDecimal nilai;
}
