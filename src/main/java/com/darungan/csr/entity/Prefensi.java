package com.darungan.csr.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class Prefensi extends BaseEntity {

    @NotNull
    @NotEmpty
    private String nama;

    @NotNull
    private int bobot;
}
