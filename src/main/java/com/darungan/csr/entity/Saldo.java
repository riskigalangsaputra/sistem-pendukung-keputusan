package com.darungan.csr.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class Saldo extends BaseEntity {

    @NotNull
    private BigDecimal jumlah = BigDecimal.ZERO;
}
