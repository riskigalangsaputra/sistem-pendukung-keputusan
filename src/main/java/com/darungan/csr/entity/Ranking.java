package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Ranking extends BaseEntity {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_pengajuan", nullable = false)
    private Pengajuan alternatif;

    @NotNull
    private float nilai;
}
