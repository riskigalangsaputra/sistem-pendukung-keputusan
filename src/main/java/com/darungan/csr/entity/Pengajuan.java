package com.darungan.csr.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class Pengajuan extends BaseEntity {

    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String kode;

    private LocalDateTime tanggalPengajuan;

    @NotNull
    @NotEmpty(message = "Nama instansi tidak boleh kosong !")
    private String namaInstansi;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_kedekatan_relasi", nullable = false)
    private SubKriteria kedekatanRelasi;

    @NotNull(message = "Alamat tidak boleh kosong !")
    @NotEmpty
    private String alamatInstansi;

    @NotNull
    @NotEmpty(message = "Nama pic tidak boleh kosong !")
    private String namaPic;

    @NotNull
    @NotEmpty(message = "Kontak pic tidak boleh kosong !")
    private String kontakPic;

    @NotNull
    @NotEmpty(message = "Gambaran kegiatan tidak boleh kosong !")
    private String gambaranKegiatan;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_jenis_kegiatan", nullable = false)
    private SubKriteria jenisKegiatan;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_lokasi_pelaksana", nullable = false)
    private SubKriteria lokasiPelaksana;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pemohon", nullable = false)
    private Pemohon pemohon;

    private String nominalAnggaran;

    private String pilihanAnggaran;

    private BigDecimal jumlahAnggaranDiperoleh = BigDecimal.ZERO;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusPengajuan statusPengajuan;

    private String suratPengantar;

    private String proposalKegiatan;

    private BigDecimal bobot = BigDecimal.ZERO;

    private String alasanPenolakan;

    public enum StatusPengajuan {
        WAITING_APPROVE, APPROVED, REJECT, SELESAI
    }
}
