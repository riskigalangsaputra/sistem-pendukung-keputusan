package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class PvAlternatif extends BaseEntity {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_pengajuan", nullable = false)
    private Pengajuan alternatif;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria", nullable = false)
    private Kriteria kriteria;

    @NotNull
    private float nilai;
}
