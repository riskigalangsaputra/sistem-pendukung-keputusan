package com.darungan.csr.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "c_security_permission")
@Data
public class Permission extends BaseEntity {

    private static final long serialVersionUID = -5239851685404647919L;

    @Size(max = 100)
    @NotEmpty(message = "label tidak boleh kosong")
    @Column(name = "permission_label", nullable = false, unique = true, length = 100)
    private String permissionLabel;

    @Size(max = 100)
    @NotEmpty(message = "value tidak boleh kosong")
    @Column(name = "permission_value", nullable = false, unique = true, length = 100)
    private String permissionValue;

}
