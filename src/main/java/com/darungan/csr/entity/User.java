package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "c_security_user")
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User extends BaseEntity {

    @Size(max = 100)
    @NotEmpty(message = "Username tidak boleh dikosongkan")
    @Column(nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private String username;

    @NotNull
    @NotEmpty(message = "Nama tidak boleh kosong")
    private String fullname;

    private Boolean active = Boolean.FALSE;

    @JsonIgnore
    @OneToOne(mappedBy = "user", optional = true)
    @Cascade(CascadeType.ALL)
    private UserPassword userPassword;

    @Transient
    @JsonIgnore
    private String password;

    @NotNull(message = "Role tidak boleh dikosongkan")
    @ManyToOne
    @JoinColumn(name = "id_role", nullable = false)
    private Role role;

    @Enumerated(EnumType.STRING)
    private UserType userType;

    public enum UserType{
        ADMIN,
        MANAGER,
        PEMOHON
    }
}
