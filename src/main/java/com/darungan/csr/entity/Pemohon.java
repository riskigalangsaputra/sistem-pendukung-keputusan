package com.darungan.csr.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
public class Pemohon extends BaseEntity {

    @NotNull
    @OneToOne
    @JoinColumn(name = "id_user", nullable = false)
    private User user;

    @NotNull @NotEmpty
    private String kode;

    @NotNull @NotEmpty(message = "Nama Telepon tidak boleh kosong !")
    private String nama;

    @NotNull @NotEmpty(message = "Email Telepon tidak boleh kosong !")
    private String email;

    @NotNull @NotEmpty(message = "Nomor Telepon tidak boleh kosong !")
    private String noTelepon;

    @NotNull @NotEmpty(message = "Alamat tidak boleh kosong !")
    private String alamat;
}
