package com.darungan.csr.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class PerbandinganAlternatif extends BaseEntity {

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_kriteria", nullable = false)
    private Kriteria pembanding;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_alternatif_satu", nullable = false)
    private Pengajuan alternatifSatu;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "id_alternatif_dua", nullable = false)
    private Pengajuan alternatifDua;

    @NotNull
    private float nilai;

}
