package com.darungan.csr.services;

import com.darungan.csr.dao.IrDao;
import com.darungan.csr.dao.KriteriaDao;
import com.darungan.csr.dao.PerbandinganKriteriaDao;
import com.darungan.csr.dao.PvectorKriteriaDao;
import com.darungan.csr.dtos.HasilPerbandinganDto;
import com.darungan.csr.dtos.KriteriaDto;
import com.darungan.csr.dtos.PerbandinganDto;
import com.darungan.csr.entity.Ir;
import com.darungan.csr.entity.Kriteria;
import com.darungan.csr.entity.PerbandinganKriteria;
import com.darungan.csr.entity.PvectorKriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
public class KriteriaService {

    private Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KriteriaDao kriteriaDao;

    @Autowired
    private PerbandinganKriteriaDao perbandinganKriteriaDao;

    @Autowired
    private PvectorKriteriaDao pvectorKriteriaDao;

    @Autowired
    private IrDao irDao;

    public Kriteria getKriteriaByIndex(int index) {
        return kriteriaDao.findByOrderByIdAsc().get(index);
    }

    public List<Kriteria> getKriteriaList() {
        List<Kriteria> list = new ArrayList<>();
        kriteriaDao.findAll().forEach(list::add);
        List<Kriteria> result = list.stream()
                .sorted(Comparator.comparing(Kriteria::getId))
                .collect(Collectors.toList());
        return result;
    }

    public Optional<PvectorKriteria> getPvKriteriaListByKriteria(String idKriteria) {
        if (pvectorKriteriaDao.findByKriteriaId(idKriteria).isPresent()) {
            return pvectorKriteriaDao.findByKriteriaId(idKriteria);
        }
        return Optional.empty();
    }

    public List<KriteriaDto> perbandinganKriteriaList() {
        List<KriteriaDto> kriteriaDtos = new ArrayList<>();
        for (int i = 0; i < getKriteriaList().size(); i++) {
            for (int k = i + 1; k < getKriteriaList().size(); k++) {
                if (getKriteriaList().get(i) != getKriteriaList().get(k)) {
                    KriteriaDto dto = new KriteriaDto();
                    dto.setKriteria1(getKriteriaList().get(i).getNama());
                    dto.setKriteria2(getKriteriaList().get(k).getNama());
                    kriteriaDtos.add(dto);
                }
            }
        }
        return kriteriaDtos;
    }

    public Optional<Kriteria> findById(String id) {
        if (StringUtils.hasText(id)) {
            return kriteriaDao.findById(id);
        }
        return Optional.empty();
    }

    public HasilPerbandinganDto prosesPerbandingan(PerbandinganDto perbandinganDto) {
        DecimalFormat df = new DecimalFormat("#.####");
        double[][] matriksKriteria = new double[getKriteriaList().size()][getKriteriaList().size()];
        int urut = 0;
        for (int x = 0; x <= (getKriteriaList().size() - 2); x++) {
            for (int y = x + 1; y <= (getKriteriaList().size() - 1); y++) {

                int pilihan = perbandinganDto.getPilihan().get(urut);
                double bobot = perbandinganDto.getBobots().get(urut);

                if (pilihan == 1) {
                    matriksKriteria[x][y] = bobot;
                    matriksKriteria[y][x] = 1 / bobot;
                } else {
                    matriksKriteria[x][y] = 1 / bobot;
                    matriksKriteria[y][x] = bobot;
                }

                urut++;
                fillPerbandinganKriteria(x, y, matriksKriteria[y][x]);
            }
        }

        //mengisi setiap pasangan kriteria yg sama dengan nilai 1
        for (int i = 0; i <= getKriteriaList().size() - 1; i++) {
            matriksKriteria[i][i] = 1;
        }

        //buat dulu matriks untuk nyimpan jumlah setiap baris dan kolom
        double[] jmlpb = new double[getKriteriaList().size()];
        double[] jmlmnk = new double[getKriteriaList().size()];

        for (int i = 0; i <= (getKriteriaList().size() - 1); i++) {
            jmlpb[i] = 0;
            jmlmnk[i] = 0;
        }

        for (int x = 0; x <= (getKriteriaList().size() - 1); x++) {
            for (int y = 0; y <= (getKriteriaList().size() - 1); y++) {
                double value = matriksKriteria[x][y];
                jmlpb[y] += value;
            }
        }

        double[][] matrikb = new double[getKriteriaList().size()][getKriteriaList().size()];
        double[] priorityVector = new double[getKriteriaList().size()];

        //menghitung nilai pvector
        for (int x = 0; x <= (getKriteriaList().size() - 1); x++) {
            for (int y = 0; y <= (getKriteriaList().size() - 1); y++) {
                matrikb[x][y] = matriksKriteria[x][y] / jmlpb[y];
                double value = matrikb[x][y];
                jmlmnk[x] += value;
            }

            priorityVector[x] = jmlmnk[x] / getKriteriaList().size();
            // insert PvectorKriteria
            fillPvectorKriteria(x, priorityVector[x]);
        }

        double eigenvektor = getEigenVector(jmlpb, jmlmnk, getKriteriaList().size());
        double consIndex = getConsIndex(jmlpb, jmlmnk, getKriteriaList().size());
        double consratio = getConsRatio(jmlpb, jmlmnk, getKriteriaList().size());

        // masukan semua hasil perhitungan ke dto
        HasilPerbandinganDto hasilDto = new HasilPerbandinganDto();
        hasilDto.setJmlpb(jmlpb);
        hasilDto.setJmlmnk(jmlmnk);
        hasilDto.setMatriks(matriksKriteria);
        hasilDto.setMatriksTernormalisasi(matrikb);
        hasilDto.setEigenValue(eigenvektor);
        hasilDto.setConsistencyIndex(consIndex);
        hasilDto.setConsistencyRatio(consratio);
        hasilDto.setPriorityVector(priorityVector);
        return hasilDto;
    }

    private double getEigenVector(double[] jmlpb, double[] jmlmnk, int n) {
        double eigenvektor = 0;
        for (int i = 0; i <= (getKriteriaList().size() - 1); i++) {
            eigenvektor += (jmlpb[i] * (jmlmnk[i] / n));
        }
        return eigenvektor;
    }

    private double getConsIndex(double[] jmlpb, double[] jmlmnk, int n) {
        double eigenvektor = getEigenVector(jmlpb, jmlmnk, n);
        double consindex = (eigenvektor - n) / (n - 1);
        return consindex;
    }

    public float getNilaiFromIr(int jumlah) {
        float nilai = 0;
        Ir data = irDao.findByJumlah(jumlah);
        if (data != null) {
            nilai = data.getNilai();
        }
        return nilai;
    }

    private double getConsRatio(double[] jmlpb, double[] jmlmnk, int n) {
        double consindex = getConsIndex(jmlpb, jmlmnk, n);
        double consratio = consindex / getNilaiFromIr(n);
        return consratio;
    }

    private void fillPvectorKriteria(int x, double priorityVector) {
        PvectorKriteria pvectorKriteria = new PvectorKriteria();
        Optional<PvectorKriteria> optional = pvectorKriteriaDao.findByKriteriaId(getKriteriaList().get(x).getId());
        if (optional.isPresent()) {
            pvectorKriteria = optional.get();
        }
        pvectorKriteria.setKriteria(getKriteriaList().get(x));
        pvectorKriteria.setNilai((float) priorityVector);
        pvectorKriteriaDao.save(pvectorKriteria);
    }

    private void fillPerbandinganKriteria(int x, int y, double matriksKriteria) {
        PerbandinganKriteria perbandinganKriteria = new PerbandinganKriteria();
        Optional<PerbandinganKriteria> opPerbandingan
                = perbandinganKriteriaDao.findByKriteriaSatuIdAndKriteriaDuaId(getKriteriaList().get(x).getId(), getKriteriaList().get(y).getId());
        if (opPerbandingan.isPresent()) {
            perbandinganKriteria = opPerbandingan.get();
        }
        perbandinganKriteria.setKriteriaSatu(getKriteriaList().get(x));
        perbandinganKriteria.setKriteriaDua(getKriteriaList().get(y));
        perbandinganKriteria.setNilai(new BigDecimal(matriksKriteria));
        perbandinganKriteriaDao.save(perbandinganKriteria);
    }
}
