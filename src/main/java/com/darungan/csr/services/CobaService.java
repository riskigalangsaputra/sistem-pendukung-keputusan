package com.darungan.csr.services;

import com.darungan.csr.entity.Coba;
import org.springframework.data.domain.Page;

import java.util.Map;

/**
 * @author galang
 */
public interface CobaService {

    Page<Coba> findAll(int page, int size, Map<String, String> params);
}
