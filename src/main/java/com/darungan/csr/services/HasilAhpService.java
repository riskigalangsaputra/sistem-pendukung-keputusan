package com.darungan.csr.services;

import com.darungan.csr.dao.PvAlternatifDao;
import com.darungan.csr.dao.PvectorKriteriaDao;
import com.darungan.csr.dao.RankingDao;
import com.darungan.csr.dtos.PeringkatDto;
import com.darungan.csr.dtos.PvAlternatifDto;
import com.darungan.csr.dtos.PvKriteriaDto;
import com.darungan.csr.dtos.RankingDto;
import com.darungan.csr.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HasilAhpService {

    @Autowired
    private KriteriaService kriteriaService;

    @Autowired
    private AlternatifService alternatifService;

    @Autowired
    private PvAlternatifDao pvAlternatifDao;

    @Autowired
    private PvectorKriteriaDao pvectorKriteriaDao;

    @Autowired
    private RankingDao rankingDao;

    public void hitungPeringkat() {
        int jmlKriteria = kriteriaService.getKriteriaList().size();
        int jmlAlternatif = alternatifService.getAlternatifList().size();
        double[] nilai = new double[jmlAlternatif];

        if (!kriteriaService.getKriteriaList().isEmpty() && !alternatifService.getAlternatifList().isEmpty()) {
            for (int x = 0; x <= (jmlAlternatif - 1); x++) {
                nilai[x] = 0;
                for (int y = 0; y <= (jmlKriteria - 1); y++) {

                    Optional<PvAlternatif> pvAlternatif = pvAlternatifDao.
                            findByAlternatifIdAndKriteriaId(
                                    alternatifService.getAlternatifList().get(x).getId(),
                                    kriteriaService.getKriteriaByIndex(y).getId());

                    Optional<PvectorKriteria> pvectorKriteria = pvectorKriteriaDao.findByKriteriaId(kriteriaService.getKriteriaByIndex(y).getId());
                    if (pvAlternatif.isPresent() && pvectorKriteria.isPresent()) {
                        double nilaiPvalternatif = pvAlternatif.get().getNilai();
                        double nilaiPvKriteria = pvectorKriteria.get().getNilai();

                        nilai[x] += (nilaiPvalternatif * nilaiPvKriteria);
                    }
                }
            }

            for (int i = 0; i <= (jmlAlternatif - 1); i++) {
                Ranking ranking = new Ranking();
                Optional<Ranking> opRanking = rankingDao.findByAlternatifId(alternatifService.getAlternatifList().get(i).getId());
                if (opRanking.isPresent()) {
                    ranking = opRanking.get();
                }
                ranking.setAlternatif(alternatifService.getAlternatifList().get(i));
                ranking.setNilai((float) nilai[i]);
                rankingDao.save(ranking);
            }
        }
    }

    public List<PvKriteriaDto> getPvKriteriaListDto() {
        List<PvKriteriaDto> listDto = new ArrayList<>();
        kriteriaService.getKriteriaList().forEach(data -> {
            Optional<PvectorKriteria> pvKriteria = kriteriaService.getPvKriteriaListByKriteria(data.getId());
            if (pvKriteria.isPresent()) {
                PvKriteriaDto pvKriteriaDto = new PvKriteriaDto();
                pvKriteriaDto.setKriteria(data);
                pvKriteriaDto.setNilai(pvKriteria.get().getNilai());
                listDto.add(pvKriteriaDto);
            }
        });
        return listDto;
    }

    public List<PvAlternatif> getPvAternatifList() {
        List<PvAlternatif> list = new ArrayList<>();
        pvAlternatifDao.findAll().forEach(list::add);
        return list;
    }

    public RankingDto getTotal() {
        float[] nilai = new float[alternatifService.getAlternatifList().size()];
        for (int y = 0; y <= (alternatifService.getAlternatifList().size() - 1); y++) {
            nilai[y] = 0;

            for (int x = 0; x <= (kriteriaService.getKriteriaList().size() - 1); x++) {
                Optional<PvAlternatif> alternatifPv = pvAlternatifDao.findByAlternatifIdAndKriteriaId(
                        alternatifService.getAlternatifList().get(y).getId(), kriteriaService.getKriteriaList().get(x).getId());

                Optional<PvectorKriteria> kriteriaPv = pvectorKriteriaDao.findByKriteriaId(kriteriaService.getKriteriaList().get(x).getId());

                nilai[y] += (alternatifPv.get().getNilai() * kriteriaPv.get().getNilai());
            }
        }
        RankingDto rankingDto = new RankingDto();
        rankingDto.setTotal(nilai);
        return rankingDto;
    }

    public RankingDto getPvAlternatifNilai() {
        double[][] nilai = new double[kriteriaService.getKriteriaList().size()][alternatifService.getAlternatifList().size()];
        for (int x = 0; x <= (kriteriaService.getKriteriaList().size() - 1); x++) {
            for (int y = 0; y <= (alternatifService.getAlternatifList().size() - 1); y++) {
                Optional<PvAlternatif> opAlternatif = pvAlternatifDao.findByAlternatifIdAndKriteriaId(alternatifService.getAlternatifList().get(y).getId(), kriteriaService.getKriteriaList().get(x).getId());
                if (opAlternatif.isPresent()) {
                    nilai[x][y] = opAlternatif.get().getNilai();

                }
            }
        }
        RankingDto rankingDto = new RankingDto();
        rankingDto.setNilai(nilai);
        return rankingDto;
    }

    public List<PeringkatDto> getPeringkatDtoList() {
        List<PeringkatDto> listDto = new ArrayList<>();
        int urut = 0;
        for (int x = 0; x <= (rankingDao.findAllByOrderByNilaiDesc().size() - 1); x++) {
            if (Pengajuan.StatusPengajuan.APPROVED.equals(rankingDao.findAllByOrderByNilaiDesc().get(x).getAlternatif().getStatusPengajuan())) {
                urut++;
                PeringkatDto dto = new PeringkatDto();
                dto.setUrut(urut);
                dto.setNama(rankingDao.findAllByOrderByNilaiDesc().get(x).getAlternatif().getNamaInstansi());
                dto.setNilai(rankingDao.findAllByOrderByNilaiDesc().get(x).getNilai());
                listDto.add(dto);
            }
        }
        return listDto;
    }
}
