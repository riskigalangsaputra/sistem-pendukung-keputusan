package com.darungan.csr.services;

import com.darungan.csr.dao.CobaDao;
import com.darungan.csr.entity.Coba;
import com.darungan.csr.specification.SpecificationBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @author galang
 */

@Service
public class CobaServiceImpl implements CobaService {

    @Autowired
    private CobaDao cobaDao;

    @Override
    public Page<Coba> findAll(int page, int size, Map<String, String> params) {
        PageRequest pageRequest = PageRequest.of(page, size);

        String nama = params.get("nama");
        String email = params.get("email");
        String negara = params.get("negara");

        SpecificationBuilder<Coba> builder = new SpecificationBuilder<>();
        if (!Strings.isEmpty(nama)) builder.with("nama", "like", nama);
        if (!Strings.isEmpty(email)) builder.with("email", "equal", email);
        if (!Strings.isEmpty(negara)) builder.with("negara", "like", negara);
        return cobaDao.findAll(pageRequest);
    }
}
