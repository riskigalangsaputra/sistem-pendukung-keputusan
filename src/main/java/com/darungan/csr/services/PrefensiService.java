package com.darungan.csr.services;

import com.darungan.csr.dao.PrefensiDao;
import com.darungan.csr.entity.Prefensi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrefensiService {

    @Autowired
    private PrefensiDao prefensiDao;

    public Iterable<Prefensi> getPrefensiIterable() {
        return prefensiDao.findAll();
    }
}
