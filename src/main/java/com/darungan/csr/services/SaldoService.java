package com.darungan.csr.services;

import com.darungan.csr.dao.SaldoDao;
import com.darungan.csr.entity.Saldo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoService {

    @Autowired
    private SaldoDao saldoDao;

    public List<Saldo> getSaldoList() {
        List<Saldo> list = new ArrayList<>();
        saldoDao.findAll().forEach(list::add);
        return list;
    }

    public Optional<Saldo> findById(String id) {
        if (StringUtils.hasText(id)) {
            return saldoDao.findById(id);
        }
        return Optional.empty();
    }

    public void resetNominal(String id) {
        Optional<Saldo> saldoOp = findById(id);
        if (saldoOp.isPresent()) {
            saldoOp.get().setJumlah(new BigDecimal(0));
            saldoDao.save(saldoOp.get());
        }
    }

    public void save(Saldo saldo) {
        Saldo saldo1 = new Saldo();
        Optional<Saldo> opSaldo = findById(saldo.getId());
        if (opSaldo.isPresent()) {
            saldo1 = opSaldo.get();
        }
        saldo1.setJumlah(saldo1.getJumlah().add(saldo.getJumlah()));
        saldoDao.save(saldo1);
    }
}
