package com.darungan.csr.services;

import com.darungan.csr.dao.PengajuanDao;
import com.darungan.csr.dao.PerbandinganAlternatifDao;
import com.darungan.csr.dao.PvAlternatifDao;
import com.darungan.csr.dtos.HasilPerbandinganDto;
import com.darungan.csr.dtos.KriteriaDto;
import com.darungan.csr.dtos.PerbandinganDto;
import com.darungan.csr.entity.Kriteria;
import com.darungan.csr.entity.Pengajuan;
import com.darungan.csr.entity.PerbandinganAlternatif;
import com.darungan.csr.entity.PvAlternatif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AlternatifService {

    @Autowired
    private PengajuanDao pengajuanDao;

    @Autowired
    private PvAlternatifDao pvAlternatifDao;

    @Autowired
    private PerbandinganAlternatifDao perbandinganAlternatifDao;

    @Autowired
    private KriteriaService kriteriaService;

    public long countWaitingApprove() {
        return pengajuanDao.countByStatusPengajuan(Pengajuan.StatusPengajuan.WAITING_APPROVE);
    }

    public long countReject() {
        return pengajuanDao.countByStatusPengajuan(Pengajuan.StatusPengajuan.REJECT);
    }

    public long countApproved() {
        return pengajuanDao.countByStatusPengajuan(Pengajuan.StatusPengajuan.APPROVED);
    }

    public long countSelesai() {
        return pengajuanDao.countByStatusPengajuan(Pengajuan.StatusPengajuan.SELESAI);
    }


    public long countWaitingApprovePemohon(String username) {
        return pengajuanDao.countByStatusPengajuanAndPemohonEmail(Pengajuan.StatusPengajuan.WAITING_APPROVE, username);
    }

    public long countRejectPemohon(String username) {
        return pengajuanDao.countByStatusPengajuanAndPemohonEmail(Pengajuan.StatusPengajuan.REJECT, username);
    }

    public long countApprovedPemohon(String username) {
        return pengajuanDao.countByStatusPengajuanAndPemohonEmail(Pengajuan.StatusPengajuan.APPROVED, username);
    }

    public long countSelesaiPemohon(String username) {
        return pengajuanDao.countByStatusPengajuanAndPemohonEmail(Pengajuan.StatusPengajuan.SELESAI, username);
    }

    public List<Pengajuan> getAlternatifList() {
        List<Pengajuan> list = new ArrayList<>();
        pengajuanDao.findByStatusPengajuan(Pengajuan.StatusPengajuan.APPROVED).forEach(list::add);
        return list;
    }

    public List<KriteriaDto> perbandinganAlternatifList() {
        List<KriteriaDto> kriteriaDtos = new ArrayList<>();
        for (int i = 0; i < getAlternatifList().size(); i++) {
            for (int k = i + 1; k < getAlternatifList().size(); k++) {
                if (getAlternatifList().get(i) != getAlternatifList().get(k)) {
                    KriteriaDto dto = new KriteriaDto();
                    dto.setKriteria1(getAlternatifList().get(i).getNamaInstansi());
                    dto.setKriteria2(getAlternatifList().get(k).getNamaInstansi());
                    kriteriaDtos.add(dto);
                }
            }
        }
        return kriteriaDtos;
    }

    private void fillPerbandinganAlternatif(int x, int y, Kriteria pemabanding, double nilai) {
        PerbandinganAlternatif perbandingan = new PerbandinganAlternatif();
        Optional<PerbandinganAlternatif> opPerbandingan =
                perbandinganAlternatifDao.findByAlternatifSatuIdAndAlternatifDuaIdAndPembandingId(getAlternatifList().get(x).getId(),
                        getAlternatifList().get(x).getId(), pemabanding.getId());

        if (opPerbandingan.isPresent()) {
            perbandingan = opPerbandingan.get();
        }
        perbandingan.setNilai((float) nilai);
        perbandingan.setAlternatifSatu(getAlternatifList().get(x));
        perbandingan.setAlternatifDua(getAlternatifList().get(y));
        perbandingan.setPembanding(pemabanding);
        perbandinganAlternatifDao.save(perbandingan);
    }

    @Transactional
    public HasilPerbandinganDto prosesPerbandinganAlternatif(PerbandinganDto perbandinganDto) {
        DecimalFormat df = new DecimalFormat("#.####");
        int n = getAlternatifList().size();
        double[][] matriksAlternatif = new double[n][n];
        int urut = 0;
        for (int x = 0; x <= (n - 2); x++) {
            for (int y = x + 1; y <= (n - 1); y++) {

                int pilihan = perbandinganDto.getPilihan().get(urut);
                double bobot = perbandinganDto.getBobots().get(urut);

                if (pilihan == 1) {
                    matriksAlternatif[x][y] = bobot;
                    matriksAlternatif[y][x] = 1 / bobot;
                } else {
                    matriksAlternatif[x][y] = 1 / bobot;
                    matriksAlternatif[y][x] = bobot;
                }

                urut++;
                fillPerbandinganAlternatif(x, y, perbandinganDto.getPembanding(), matriksAlternatif[x][y]);
            }
        }

        //mengisi setiap pasangan kriteria yg sama dengan nilai 1
        for (int i = 0; i <= n - 1; i++) {
            matriksAlternatif[i][i] = 1;
        }

        double[] jmlpb = new double[n];
        double[] jmlmnk = new double[n];

        for (int i = 0; i <= (n - 1); i++) {
            jmlpb[i] = 0;
            jmlmnk[i] = 0;
        }

        for (int x = 0; x <= (n - 1); x++) {
            for (int y = 0; y <= (n - 1); y++) {
                double value = matriksAlternatif[x][y];
                jmlpb[y] += value;
            }
        }

        double[][] matrikb = new double[n][n];
        double[] priorityVector = new double[n];

        for (int x = 0; x <= (n - 1); x++) {
            for (int y = 0; y <= (n - 1); y++) {
                matrikb[x][y] = matriksAlternatif[x][y] / jmlpb[y];
                double value = matrikb[x][y];
                jmlmnk[x] += value;
            }

            priorityVector[x] = jmlmnk[x] / n;
            // insert PvectorAlternatif
            fillPvectorAlternatif(x, perbandinganDto.getPembanding(), priorityVector[x]);
        }

        double eigenvektor = getEigenVector(jmlpb, jmlmnk, n);
        double consIndex = getConsIndex(jmlpb, jmlmnk, n);
        double consratio = getConsRatio(jmlpb, jmlmnk, n);

        HasilPerbandinganDto hasilDto = new HasilPerbandinganDto();
        hasilDto.setJmlpb(jmlpb);
        hasilDto.setJmlmnk(jmlmnk);
        hasilDto.setMatriks(matriksAlternatif);
        hasilDto.setMatriksTernormalisasi(matrikb);
        hasilDto.setEigenValue(eigenvektor);
        hasilDto.setConsistencyIndex(consIndex);
        hasilDto.setConsistencyRatio(consratio);
        hasilDto.setPriorityVector(priorityVector);
        return hasilDto;
    }

    private double getEigenVector(double[] jmlpb, double[] jmlmnk, int n) {
        double eigenvektor = 0;
        for (int i = 0; i <= (n - 1); i++) {
            eigenvektor += (jmlpb[i] * (jmlmnk[i] / n));
        }
        return eigenvektor;
    }

    private double getConsIndex(double[] jmlpb, double[] jmlmnk, int n) {
        double eigenvektor = getEigenVector(jmlpb, jmlmnk, n);
        double consindex = (eigenvektor - n) / (n - 1);
        return consindex;
    }

    private double getConsRatio(double[] jmlpb, double[] jmlmnk, int n) {
        double consindex = getConsIndex(jmlpb, jmlmnk, n);
        double consratio = consindex / kriteriaService.getNilaiFromIr(n);
        return consratio;
    }

    private void fillPvectorAlternatif(int x, Kriteria kriteria, double priorityVector) {
        PvAlternatif pvAlternatif = new PvAlternatif();
        Optional<PvAlternatif> opPv = pvAlternatifDao.findByAlternatifIdAndKriteriaId(getAlternatifList().get(x).getId(), kriteria.getId());
        if (opPv.isPresent()) {
            pvAlternatif = opPv.get();
        }

        pvAlternatif.setAlternatif(getAlternatifList().get(x));
        pvAlternatif.setKriteria(kriteria);
        pvAlternatif.setNilai((float) priorityVector);
        pvAlternatifDao.save(pvAlternatif);
    }
}
