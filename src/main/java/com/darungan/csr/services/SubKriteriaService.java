package com.darungan.csr.services;

import com.darungan.csr.dao.SubKriteriaDao;
import com.darungan.csr.entity.SubKriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubKriteriaService {

    @Autowired
    private SubKriteriaDao subKriteriaDao;

    public List<SubKriteria> getLokasiKegiatanList() {
        return subKriteriaDao.findByKriteriaId("k-01");
    }

    public List<SubKriteria> getKedekatanRelasiList() {
        return subKriteriaDao.findByKriteriaId("k-02");
    }

    public List<SubKriteria> getJenisKegiatanList() {
        return subKriteriaDao.findByKriteriaId("k-03");
    }

    public Page<SubKriteria> getSubKriteriaPage(Pageable pageable) {
        return subKriteriaDao.findAll(pageable);
    }

    private void validate(SubKriteria subKriteria) throws Exception {
        Optional<SubKriteria> opSub = subKriteriaDao.findByKriteriaIdAndNama(subKriteria.getId(), subKriteria.getNama());
        if (opSub.isPresent()) {
            throw new IllegalArgumentException("Data duplicate !");
        }
    }

    public void save(SubKriteria subKriteria) throws Exception {
        validate(subKriteria);
        subKriteriaDao.save(subKriteria);
    }

}
