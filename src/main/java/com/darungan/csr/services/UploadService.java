package com.darungan.csr.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class UploadService {

    private final Logger logger = LoggerFactory.getLogger(UploadService.class);

    @Value("${app.folder.upload}")
    public String uploadDir;

    private static final SimpleDateFormat YYYYMMDDHHMMSS = new SimpleDateFormat("yyyyMMddHHmmss");

    public static final String ORIGINAL_PREFIX = "ori-";

    public static final String PENGAJUAN_ATTACHMENT = "file";

    public File moveDocument(MultipartFile multipartFile, String type, String extension) {
        try {

            String folder = uploadDir + File.separator + type + File.separator;
            File folderImages = new File(folder);
            if (!folderImages.exists()) {
                logger.debug("Creating folder [{}]", folderImages.getAbsolutePath());
                Files.createDirectories(folderImages.toPath());
            }


            String random = YYYYMMDDHHMMSS.format(new Date());
            logger.debug("random [{}]", random);
            logger.debug("multipartFile [{}]", multipartFile.getSize());


            String fileName = multipartFile.getOriginalFilename();
            int occurance = StringUtils.countOccurrencesOf(fileName, ".");
            if (occurance > 1) {
                for (int i = 0; i < occurance - 1; i++) {
                    fileName = fileName.replaceFirst("\\.", "-");

                }
            }

            fileName = fileName.replace(" ", "-");
            fileName = fileName.replace("_", "-");
            fileName = fileName.replaceAll("[^\\w\\-\\.]", "");
            String name = random + "-" + fileName;

            File originalFile = new File(folder + ORIGINAL_PREFIX + name);
            logger.debug("original File [{}]", originalFile.getPath());
            Files.copy(multipartFile.getInputStream(), originalFile.toPath());
            return originalFile;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
}
