package com.darungan.csr.services;

import com.darungan.csr.dao.PemohonDao;
import com.darungan.csr.dao.PengajuanDao;
import com.darungan.csr.dao.SaldoDao;
import com.darungan.csr.dtos.PengajuanDto;
import com.darungan.csr.dtos.SaldoDto;
import com.darungan.csr.dtos.TolakDto;
import com.darungan.csr.entity.BaseEntity;
import com.darungan.csr.entity.Pemohon;
import com.darungan.csr.entity.Pengajuan;
import com.darungan.csr.entity.Saldo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.expression.Strings;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Transactional
@Service
public class PengajuanService extends BaseEntity {

    @Autowired
    private PengajuanDao pengajuanDao;

    @Autowired
    private PemohonDao pemohonDao;

    @Autowired
    private SaldoDao saldoDao;

    public Page<Pengajuan> getPengajuanPage(Pageable pageable) {
        return pengajuanDao.findAll(pageable);
    }

    public Page<Pengajuan> getPengajuanPageByUser(String username, Pageable pageable) {
        return pengajuanDao.findByPemohonUserUsername(username, pageable);
    }

    private void validateForm(String username, Pengajuan pengajuan, PengajuanDto pengajuanDto,
                              String suratPengantar, String proposalKegiatan) throws Exception {
        List<Pengajuan> pengajuans = pengajuanDao.findByPemohonUserUsername(username);
        if (!pengajuans.isEmpty()) {
            pengajuans.forEach(data -> {
                if (pengajuan != null && !pengajuanDto.getId().equals(pengajuan.getId()) && !Pengajuan.StatusPengajuan.SELESAI.equals(data.getStatusPengajuan()) && !Pengajuan.StatusPengajuan.REJECT.equals(data.getStatusPengajuan())) {
                    throw new IllegalStateException("Tidak dapat melakukan pengajuan dana, Karena ada pengajuan yang sedang diproses.");
                }
            });
        }

        if (!StringUtils.hasText(suratPengantar) && !StringUtils.hasText(pengajuanDto.getId())) {
            throw new IllegalStateException("Surat pengantar belum di upload");
        }

        if (!StringUtils.hasText(proposalKegiatan) && !StringUtils.hasText(pengajuanDto.getId())) {
            throw new IllegalStateException("Proposal kegiatan belum di upload");
        }

        BigDecimal nominal = new BigDecimal(pengajuanDto.getNominalAnggaran());

        if ("Kurang_dari_10_jt".equals(pengajuanDto.getPilihanAnggaran())) {
            if (nominal.compareTo(new BigDecimal(10000000)) > 0) {
                throw new IllegalStateException("Nominal tidak boleh lebih dari Rp.10.000.000");
            }
        } else if ("Kurang_dari_25_jt".equals(pengajuanDto.getPilihanAnggaran())) {
            if (nominal.compareTo(new BigDecimal(25000000)) > 0) {
                throw new IllegalStateException("Nominal tidak boleh lebih dari Rp.25.000.000");
            }
        } else if ("Kurang_dari_50_jt".equals(pengajuanDto.getPilihanAnggaran())) {
            if (nominal.compareTo(new BigDecimal(50000000)) > 0) {
                throw new IllegalStateException("Nominal tidak boleh lebih dari Rp.50.000.000");
            }
        } else if ("Lebih_dari_50_jt".equals(pengajuanDto.getPilihanAnggaran())) {
            if (nominal.compareTo(new BigDecimal(50000000)) < 0) {
                throw new IllegalStateException("Nominal tidak boleh kurang dari Rp.50.000.000");
            }
        }
    }

    public PengajuanDto getPengajuanDto(String id) {
        Pengajuan pengajuan = getPengajuanDtoById(id);
        PengajuanDto pengajuanDto = new PengajuanDto();
        BeanUtils.copyProperties(pengajuan, pengajuanDto);
        return pengajuanDto;
    }

    public Pengajuan getPengajuanDtoById(String id) {
        Pengajuan pengajuan = new Pengajuan();
        if (StringUtils.hasText(id)) {
            Optional<Pengajuan> opPengajuan = pengajuanDao.findById(id);
            if (opPengajuan.isPresent()) {
                pengajuan = opPengajuan.get();
            }
        }
        return pengajuan;
    }

    public void savePengajuan(PengajuanDto pengajuanDto, String username, String suratPengantar, String
            proposalKegiatan) throws Exception {
        Optional<Pemohon> pemohon = pemohonDao.findByEmail(username);
        if (!pemohon.isPresent()) {
            throw new IllegalStateException("Data pemohon tidak ditemukan");
        }

        Pengajuan pengajuan = new Pengajuan();
        Optional<Pengajuan> pengajuanOp = pengajuanDao.findById(pengajuanDto.getId());
        if (pengajuanOp.isPresent()) {
            pengajuan = pengajuanOp.get();
        } else {
            pengajuan.setKode("PJ-" + generateRandomCode());
        }

        validateForm(pemohon.get().getUser().getUsername(), pengajuan, pengajuanDto, suratPengantar, proposalKegiatan);
        pengajuan.setTanggalPengajuan(LocalDateTime.now());
        pengajuan.setNamaInstansi(pengajuanDto.getNamaInstansi());
        pengajuan.setKedekatanRelasi(pengajuanDto.getKedekatanRelasi());
        pengajuan.setAlamatInstansi(pengajuanDto.getAlamatInstansi());
        pengajuan.setAlamatInstansi(pengajuanDto.getAlamatInstansi());
        pengajuan.setNamaPic(pengajuanDto.getNamaPic());
        pengajuan.setKontakPic(pengajuanDto.getKontakPic());
        pengajuan.setGambaranKegiatan(pengajuanDto.getGambaranKegiatan());
        pengajuan.setJenisKegiatan(pengajuanDto.getJenisKegiatan());
        pengajuan.setLokasiPelaksana(pengajuanDto.getLokasiPelaksana());
        pengajuan.setPemohon(pemohon.get());
        pengajuan.setNominalAnggaran(pengajuanDto.getNominalAnggaran());
        pengajuan.setPilihanAnggaran(pengajuanDto.getPilihanAnggaran());
        pengajuan.setStatusPengajuan(Pengajuan.StatusPengajuan.WAITING_APPROVE);

        if (StringUtils.hasText(suratPengantar) && StringUtils.hasText(proposalKegiatan)) {
            pengajuan.setSuratPengantar(suratPengantar);
            pengajuan.setProposalKegiatan(proposalKegiatan);
        }
        pengajuanDao.save(pengajuan);
    }

    public void approve(String id) {
        if (StringUtils.hasText(id)) {
            Optional<Pengajuan> opPengajuan = pengajuanDao.findById(id);
            if (opPengajuan.isPresent()) {
                Pengajuan pengajuan = opPengajuan.get();
                pengajuan.setStatusPengajuan(Pengajuan.StatusPengajuan.APPROVED);
                pengajuanDao.save(pengajuan);
            }
        }
    }

    public void tolak(TolakDto dto) {
        Optional<Pengajuan> opPengajuan = pengajuanDao.findById(dto.getId());
        if (opPengajuan.isPresent()) {
            Pengajuan pengajuan = opPengajuan.get();
            pengajuan.setStatusPengajuan(Pengajuan.StatusPengajuan.REJECT);
            pengajuan.setAlasanPenolakan(dto.getAlasan());
            pengajuanDao.save(pengajuan);
        }
    }

    public void inputDana(SaldoDto saldoDto) throws Exception {
        saldoDao.findAll().forEach(data -> {
            if (saldoDto.getJumlah().compareTo(data.getJumlah()) > 0) {
                throw new IllegalStateException("Saldo CSR tidak cukup !");
            }
            data.setJumlah(data.getJumlah().subtract(saldoDto.getJumlah()));
            saldoDao.save(data);
        });

        Optional<Pengajuan> pengajuan = pengajuanDao.findById(saldoDto.getId());
        if (pengajuan.isPresent()) {
            pengajuan.get().setJumlahAnggaranDiperoleh(saldoDto.getJumlah());
            pengajuan.get().setStatusPengajuan(Pengajuan.StatusPengajuan.SELESAI);
            pengajuanDao.save(pengajuan.get());
        }
    }

    public void delete(String id) throws Exception {
        try {
            pengajuanDao.deleteById(id);
        } catch (Exception e) {
            throw new IllegalStateException("Pengajuan tidak bisa di hapus !");
        }
    }

    protected String generateRandomCode() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
