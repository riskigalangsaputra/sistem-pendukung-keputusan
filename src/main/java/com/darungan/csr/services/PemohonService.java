package com.darungan.csr.services;

import com.darungan.csr.dao.PemohonDao;
import com.darungan.csr.dao.UserDao;
import com.darungan.csr.dtos.PemohonDto;
import com.darungan.csr.entity.Pemohon;
import com.darungan.csr.entity.Role;
import com.darungan.csr.entity.User;
import com.darungan.csr.entity.UserPassword;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Service
@Slf4j
public class PemohonService {

    private static final String ROLE_FOR_PEMOHON = "role_for_pemohon";

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PemohonDao pemohonDao;

    public Iterable<Pemohon> getListPemohon() {
        return pemohonDao.findAll();
    }

    public void validateRegistrasi(PemohonDto pemohonDto, BindingResult bindingResult) {
        Optional<User> dupeEmail = userDao.findByUsername(pemohonDto.getEmail());
        if (dupeEmail.isPresent()) {
            bindingResult.addError(new FieldError("pemohonDto", "email", "Email sudah digunakan."));
        }

        Optional<Pemohon> dupPhone = pemohonDao.findByNoTelepon(pemohonDto.getNoTelepon());
        if (dupPhone.isPresent()) {
            bindingResult.addError(new FieldError("pemohonDto", "noTelepon", "No. Telepon/Whatsapp sudah digunakan."));
        }
    }

    public void registrasiPemohon(PemohonDto pemohonDto) throws Exception {

        User user = fillUser(pemohonDto);
        Pemohon pemohon = new Pemohon();
        pemohon.setUser(user);
        pemohon.setKode("PM-" + UUID.randomUUID().toString());
        pemohon.setNama(pemohonDto.getNama());
        pemohon.setEmail(pemohonDto.getEmail());
        pemohon.setNoTelepon(pemohonDto.getNoTelepon());
        pemohon.setAlamat(pemohonDto.getAlamat());
        pemohonDao.save(pemohon);
    }

    private User fillUser(PemohonDto pemohonDto) {
        Role role = new Role();
        role.setId(ROLE_FOR_PEMOHON);

        User user = new User();
        user.setUsername(pemohonDto.getEmail());
        user.setFullname(pemohonDto.getNama());
        user.setUserType(User.UserType.PEMOHON);
        user.setActive(Boolean.TRUE);
        user.setRole(role);

        UserPassword userPassword = new UserPassword();
        userPassword.setUser(user);
        userPassword.setPassword(passwordEncoder.encode(pemohonDto.getPassword()));

        user.setUserPassword(userPassword);
        userDao.save(user);
        return user;
    }
}
