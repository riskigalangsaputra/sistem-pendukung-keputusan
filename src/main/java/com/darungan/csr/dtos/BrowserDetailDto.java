package com.darungan.csr.dtos;

import lombok.Data;

@Data
public class BrowserDetailDto {
    private String operatingSystem;
    private String browserName;
}
