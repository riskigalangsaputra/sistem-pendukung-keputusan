package com.darungan.csr.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class KriteriaDto {
    private String kriteria1;
    private String kriteria2;
    private String radio;
    private BigDecimal nilai;
}
