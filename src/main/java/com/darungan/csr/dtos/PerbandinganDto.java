package com.darungan.csr.dtos;

import com.darungan.csr.entity.Kriteria;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PerbandinganDto {
    private List<Integer> bobots = new ArrayList<>();
    private List<Integer> pilihan = new ArrayList<>();
    private int index;
    private Kriteria pembanding;
}
