package com.darungan.csr.dtos;

import lombok.Data;

@Data
public class PeringkatDto {

    private int urut;
    private String nama;
    private float nilai;
}
