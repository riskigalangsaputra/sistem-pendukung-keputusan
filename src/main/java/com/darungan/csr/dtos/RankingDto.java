package com.darungan.csr.dtos;

import lombok.Data;

@Data
public class RankingDto {
    private double[][] nilai;
    private float[] total;

}
