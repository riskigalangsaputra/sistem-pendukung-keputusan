package com.darungan.csr.dtos;

import lombok.Getter;
import lombok.Setter;

/**
 * @author galang
 */
@Getter @Setter
public class DashboardTotalDto {

    private int totalBuku;
    private int totalPencil;
    private int totalPenggaris;
}
