package com.darungan.csr.dtos;

import com.darungan.csr.entity.Pengajuan;
import com.darungan.csr.entity.SubKriteria;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PengajuanDto {

    private String id;

    @NotNull
    @NotEmpty(message = "Nama instansi tidak boleh kosong !")
    private String namaInstansi;

    @NotNull
    private SubKriteria kedekatanRelasi;

    @NotNull(message = "Alamat tidak boleh kosong !")
    @NotEmpty
    private String alamatInstansi;

    @NotNull
    @NotEmpty(message = "Nama pic tidak boleh kosong !")
    private String namaPic;

    @NotNull
    @NotEmpty(message = "Kontak pic tidak boleh kosong !")
    private String kontakPic;

    @NotNull
    @NotEmpty(message = "Gambaran kegiatan tidak boleh kosong !")
    private String gambaranKegiatan;

    @NotNull
    private SubKriteria jenisKegiatan;

    @NotNull
    private SubKriteria lokasiPelaksana;

    private String suratPengantar;

    private String proposalKegiatan;

    @NotNull
    @NotEmpty(message = "Tidak boleh kosong !")
    private String nominalAnggaran;

    @NotNull
    @NotEmpty(message = "Tiidak boleh kosong !")
    private String pilihanAnggaran;

}
