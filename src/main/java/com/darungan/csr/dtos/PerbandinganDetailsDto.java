package com.darungan.csr.dtos;

import lombok.Data;

@Data
public class PerbandinganDetailsDto {
    private double[] pilih;
    private double[] boot;
}
