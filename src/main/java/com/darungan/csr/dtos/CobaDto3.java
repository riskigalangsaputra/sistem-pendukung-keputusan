package com.darungan.csr.dtos;

import com.darungan.csr.entity.Pengajuan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author galang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CobaDto3 {

    private Pengajuan alternatif;
    private float nilai;
}
