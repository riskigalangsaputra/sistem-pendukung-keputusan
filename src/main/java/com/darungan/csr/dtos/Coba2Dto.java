package com.darungan.csr.dtos;

import lombok.Getter;
import lombok.Setter;

/**
 * @author galang
 */
@Getter
@Setter
public class Coba2Dto {
    private String status;
    private int value;
}
