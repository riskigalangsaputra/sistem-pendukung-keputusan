package com.darungan.csr.dtos;

import com.darungan.csr.entity.Pengajuan;
import lombok.Data;

@Data
public class PvAlternatifDto {
    private Pengajuan pengajuan;
    private float nilai;
}
