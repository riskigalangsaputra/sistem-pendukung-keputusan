package com.darungan.csr.dtos;

import com.darungan.csr.entity.Kriteria;
import lombok.Data;

@Data
public class PvKriteriaDto {

    private Kriteria kriteria;
    private float nilai;
}
