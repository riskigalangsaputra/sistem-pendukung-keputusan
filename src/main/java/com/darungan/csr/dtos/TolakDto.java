package com.darungan.csr.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TolakDto {

    private String id;

    @NotNull
    @NotEmpty(message = "Mohon untuk diisi !")
    private String alasan;
}
