package com.darungan.csr.dtos;

import lombok.Data;

@Data
public class HasilPerbandinganDto {
    private double[][] matriks;
    private double[][] matriksTernormalisasi;
    private double[] priorityVector;
    private double eigenValue;
    private double consistencyIndex;
    private double consistencyRatio;
    private double[] jmlpb;
    private double[] jmlmnk;
}
