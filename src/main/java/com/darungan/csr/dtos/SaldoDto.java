package com.darungan.csr.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class SaldoDto {
    private String id;

    @NotNull
    private BigDecimal jumlah;
}
