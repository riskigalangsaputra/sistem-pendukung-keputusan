package com.darungan.csr.dtos;

import lombok.Getter;
import lombok.Setter;

/**
 * @author galang
 */
@Getter @Setter
public class CobaDto {

    private String status;
    private int total;

    private Long ttl;
}
