package com.darungan.csr.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PemohonDto {

    @NotNull
    @NotEmpty(message = "Nama Telepon tidak boleh kosong !")
    private String nama;

    @NotNull @NotEmpty(message = "Email Telepon tidak boleh kosong !")
    private String email;

    @NotNull @NotEmpty(message = "Nomor Telepon tidak boleh kosong !")
    private String noTelepon;

    @NotNull @NotEmpty(message = "Alamat tidak boleh kosong !")
    private String password;

    @NotNull @NotEmpty(message = "Alamat tidak boleh kosong !")
    private String alamat;


}
