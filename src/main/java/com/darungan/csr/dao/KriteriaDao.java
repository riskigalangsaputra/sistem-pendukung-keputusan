package com.darungan.csr.dao;

import com.darungan.csr.entity.Kriteria;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KriteriaDao extends PagingAndSortingRepository<Kriteria, String> {

    List<Kriteria> findByOrderByIdAsc();
}
