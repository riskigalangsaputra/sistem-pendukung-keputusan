package com.darungan.csr.dao;

import com.darungan.csr.entity.Pemohon;
import com.darungan.csr.entity.Pengajuan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface PengajuanDao extends PagingAndSortingRepository<Pengajuan, String> {

    Page<Pengajuan> findByPemohonUserUsername(String username, Pageable pageable);

    List<Pengajuan> findByStatusPengajuan(Pengajuan.StatusPengajuan status);

    List<Pengajuan> findByPemohonUserUsername(String username);

    long countByStatusPengajuan(Pengajuan.StatusPengajuan status);

    long countByStatusPengajuanAndPemohonEmail(Pengajuan.StatusPengajuan status, String email);

}
