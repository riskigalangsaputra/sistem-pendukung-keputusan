package com.darungan.csr.dao;

import com.darungan.csr.entity.PerbandinganKriteria;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.Optional;

public interface PerbandinganKriteriaDao extends PagingAndSortingRepository<PerbandinganKriteria, String> {

    Optional<PerbandinganKriteria> findByKriteriaSatuIdAndKriteriaDuaId(String kritSatu, String kritDua);
}
