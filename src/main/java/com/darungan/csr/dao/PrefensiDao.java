package com.darungan.csr.dao;

import com.darungan.csr.entity.Prefensi;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PrefensiDao extends PagingAndSortingRepository<Prefensi, String> {
}
