package com.darungan.csr.dao;

import com.darungan.csr.entity.SubKriteria;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface SubKriteriaDao extends PagingAndSortingRepository<SubKriteria, String> {

    List<SubKriteria> findByKriteriaId(String id);

    Optional<SubKriteria> findByKriteriaIdAndNama(String id, String nama);
}
