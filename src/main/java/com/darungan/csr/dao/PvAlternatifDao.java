package com.darungan.csr.dao;

import com.darungan.csr.entity.PvAlternatif;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PvAlternatifDao extends CrudRepository<PvAlternatif, String> {

    Optional<PvAlternatif> findByAlternatifIdAndKriteriaId(String alternatifId, String kriteriaId);
}
