package com.darungan.csr.dao;

import com.darungan.csr.entity.Coba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author galang
 */
public interface CobaDao extends JpaRepository<Coba, String>, JpaSpecificationExecutor<Coba> {
}
