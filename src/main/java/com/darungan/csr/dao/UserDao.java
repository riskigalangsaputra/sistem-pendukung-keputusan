package com.darungan.csr.dao;

import com.darungan.csr.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserDao extends PagingAndSortingRepository<User, String> {

    Optional<User> findByUsernameAndActive(String username, boolean b);

    Optional<User> findByUsername(String email);
}
