package com.darungan.csr.dao;

import com.darungan.csr.dtos.CobaDto3;
import com.darungan.csr.entity.Ranking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RankingDao extends CrudRepository<Ranking, String> {

    Optional<Ranking> findByAlternatifId(String id);

    List<Ranking> findAllByOrderByNilaiDesc();

    @Query("SELECT new com.darungan.csr.dtos.CobaDto3(a.alternatif, a.nilai) FROM Ranking a")
    List<CobaDto3> findBCoba();


    Page<Ranking> findByNilaiLike(float nilai, Pageable pageable);
}
