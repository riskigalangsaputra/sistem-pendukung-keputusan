package com.darungan.csr.dao;

import com.darungan.csr.entity.Saldo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SaldoDao extends CrudRepository<Saldo, String> {

    @Query(value = "SELECT SUM(jumlah) FROM saldo", nativeQuery = true)
    long countByJumlah();
}
