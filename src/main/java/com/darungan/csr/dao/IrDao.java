package com.darungan.csr.dao;

import com.darungan.csr.entity.Ir;
import org.springframework.data.repository.CrudRepository;

public interface IrDao extends CrudRepository<Ir, Integer> {

    Ir findByJumlah(int jumlah);
}
