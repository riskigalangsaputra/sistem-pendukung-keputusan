package com.darungan.csr.dao;

import com.darungan.csr.entity.PvectorKriteria;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface PvectorKriteriaDao extends PagingAndSortingRepository<PvectorKriteria, String> {

    Optional<PvectorKriteria> findByKriteriaId(String kriteriaId);
}
