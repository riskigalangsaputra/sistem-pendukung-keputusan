package com.darungan.csr.dao;

import com.darungan.csr.entity.Pemohon;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PemohonDao extends PagingAndSortingRepository<Pemohon, String> {

    Optional<Pemohon> findByNoTelepon(String noTelepon);

    Optional<Pemohon> findByEmail(String email);
}
