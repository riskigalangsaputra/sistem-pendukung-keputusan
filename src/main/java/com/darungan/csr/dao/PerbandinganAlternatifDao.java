package com.darungan.csr.dao;

import com.darungan.csr.entity.PerbandinganAlternatif;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PerbandinganAlternatifDao extends CrudRepository<PerbandinganAlternatif, String> {

    Optional<PerbandinganAlternatif> findByAlternatifSatuIdAndAlternatifDuaIdAndPembandingId(String alternatifSatu, String alternatifDua, String pembanding);
}
