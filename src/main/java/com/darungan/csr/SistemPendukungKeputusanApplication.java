package com.darungan.csr;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@EnableAsync
@SpringBootApplication
public class SistemPendukungKeputusanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemPendukungKeputusanApplication.class, args);
	}

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }

    @Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

}
