package com.darungan.csr.configuration;

import com.darungan.csr.dao.SaldoDao;
import com.darungan.csr.dao.UserDao;
import com.darungan.csr.entity.Saldo;
import com.darungan.csr.entity.User;
import com.darungan.csr.services.AlternatifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Optional;

@ControllerAdvice(basePackages = {"com.darungan.csr.controller"})
public class GlobalControllerAdvice {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SaldoDao saldoDao;

    @Autowired
    private AlternatifService alternatifService;

    @ModelAttribute
    public void globalAttributes(Model model, Principal principal, Authentication authentication, HttpSession session) {
        if (principal != null) {
            Optional<User> user = userDao.findByUsername(principal.getName());
            model.addAttribute("userLoggedIn", user.get());
            model.addAttribute("saldo_csr", saldoDao.countByJumlah());
            model.addAttribute("waitingApprove", alternatifService.countWaitingApprove());
        }
    }
}
