package com.darungan.csr;

import com.darungan.csr.dao.KriteriaDao;
import com.darungan.csr.dao.PerbandinganKriteriaDao;
import com.darungan.csr.dao.RankingDao;
import com.darungan.csr.dtos.CobaDto;
import com.darungan.csr.dtos.DashboardTotalDto;
import com.darungan.csr.dtos.KriteriaDto;
import com.darungan.csr.entity.Coba;
import com.darungan.csr.entity.Kriteria;
import com.darungan.csr.entity.PerbandinganKriteria;
import com.darungan.csr.services.CobaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.fields.ExcludeFieldEnumSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

    import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class SistemPendukungKeputusanApplicationTests {
    @Autowired
    private CobaService cobaService;
//    @Autowired
//    RankingDao rankingDao;
//    @Test
//    void contextLoads() {
//        System.out.println("Result : " + convertNamaBulanIndo(LocalDateTime.now()));
//    }
//
//    public static String convertNamaBulanIndo(LocalDateTime date) {
//        String bulan = String.valueOf(date.getMonth().getValue());
//        String mergeDate = getNameMonth(bulan) + " " + date.getYear();
//        return mergeDate;
//    }
//
//    public static String getNameMonth(String month) {
//        String name = "";
//        switch (month) {
//            case "01":
//                name = "JANUARI";
//                break;
//            case "02":
//                name = "FEBRUARI";
//                break;
//            case "03":
//                name = "MAR";
//                break;
//            case "04":
//                name = "APR";
//                break;
//            case "05":
//                name = "MEI";
//                break;
//            case "06":
//                name = "JUN";
//                break;
//            case "07":
//                name = "JUL";
//                break;
//            case "08":
//                name = "AGU";
//                break;
//            case "09":
//                name = "SEP";
//                break;
//            case "10":
//                name = "OKT";
//                break;
//            case "11":
//                name = "NOVEMBER";
//                break;
//            case "12":
//                name = "DESESEMBER";
//                break;
//            default:
//                name = "";
//                break;
//        }
//        return name;
//    }
//
//    @Test
//    void resultCount() {
//        List<CobaDto> data1 = new ArrayList<>();
//        List<DashboardTotalDto> results = new ArrayList<>();
//
//        CobaDto bukuDto = new CobaDto();
//        bukuDto.setStatus("Buku");
//        bukuDto.setTotal(2);
//
//        CobaDto buku2Dto = new CobaDto();
//        buku2Dto.setStatus("Buku");
//        buku2Dto.setTotal(3);
//
//        CobaDto penscilDto = new CobaDto();
//        penscilDto.setStatus("Pencil");
//        penscilDto.setTotal(1);
//
//        CobaDto penggarisDto = new CobaDto();
//        penggarisDto.setStatus("Penggaris");
//        penggarisDto.setTotal(7);
//
//        data1.add(0, bukuDto);
//        data1.add(1, penscilDto);
//        data1.add(2, buku2Dto);
//        data1.add(3, penggarisDto);
//
////        System.out.println("#### Data sebelum di grouping ####");
//        for (CobaDto cobaDto : data1) {
//            System.out.println(cobaDto.getStatus() + " : " + cobaDto.getTotal());
//        }
//
////        Long reslt = Long.parseLong(CobaDto::getTtl);
//
//
//        Map<String, Long> coba2 = data1.stream().filter(d -> !d.getStatus().equals("Pencil") && !d.getStatus().equals("Penggaris")).collect(
//                Collectors.groupingBy(CobaDto::getStatus, Collectors.summingLong(CobaDto::getTtl)));
//
//        System.out.println("================== BATAS ========================");
//        Long sum = coba2.values().stream().mapToLong(Long::valueOf).sum();
//
//
//
//        System.out.println("RESULT : " + sum);
//        DashboardTotalDto dto = new DashboardTotalDto();
////        for (Map.Entry<String, Integer> pair : coba2.entrySet()) {
////            System.out.println("Data : " + pair.getKey() + " : " + pair.getValue());
//////            if ("Buku".equals(pair.getKey())) {
//////                dto.setTotalBuku(pair.getValue());
//////            } else if ("Pencil".equals(pair.getKey())) {
//////                dto.setTotalPencil(pair.getValue());
//////            } else if ("Penggaris".equals(pair.getKey())) {
//////                dto.setTotalPenggaris(pair.getValue());
//////            }
////        }
//        results.add(dto);
//
////        System.out.println("#### Data setelah di grouping ####");
////        for (DashboardTotalDto result : results) {
////            System.out.println("Total Buku: " + result.getTotalBuku());
////            System.out.println("Total Pencil: " + result.getTotalPencil());
////            System.out.println("Total Penggaris: " + result.getTotalPenggaris());
////        }
//    }

//    @Test
//    void coba3() {
//        rankingDao.findBCoba();
//    }

//    private Page<CobaDto> convertEntityToDto(float nilai, Pageable pageable) {
//        Page<CobaDto> entities =
//                rankingDao.findByNilaiLike(nilai, pageable)
//                        .map(CobaDto::);
//        return entities;
//    }

    @Test
    void cobass() throws IOException {
        System.out.println("test aja");
    }

    private List<String> list(int limit) throws IOException {
        try {
            URL url = new URL("https://jsonmock.hackerrank.com/api/articles?page=0");
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setDoOutput(true);
            con.setReadTimeout(30_000);

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

            List<String> result = new ArrayList<>();

            String line = br.readLine();

            String[] sp = line.split("\"title\"");
            for (int i = 1; i < sp.length; i++) {
                String title = sp[i];
                String value;
                if (title.startsWith(":null")) {
                    value = title.split("story_title\":")[1].split("\"")[1];
                } else {
                    value = title.split("\"")[1];
                }
                result.add(value);
                System.out.println(value);
            }
            return result;
        } catch (Throwable e) {
            return null;
        }
    }

    public Page<Coba> findAll(int page, int size, @RequestParam Map<String, String> params) {
        return cobaService.findAll(page, size, params);
    }

}
